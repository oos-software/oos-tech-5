/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <string>
#include <vector>
#include <fstream>
#include <cctype>


namespace oos {
    static constexpr int  TokenPoolChunkSize = 2048;
    static constexpr int  ConfigEntryPoolChunkSize = 2048;
    

    //
    // read_bytes
    //

    bool read_bytes(std::vector<uint8_t> &in_buffer, std::string const &in_fileName) {
        std::ifstream  stream(in_fileName.c_str(), std::ifstream::binary);
        if (!stream.is_open()) {
            return(false);
        }

        stream.seekg(0, std::ifstream::end);
        in_buffer.resize(stream.tellg());
        stream.seekg(0, std::ifstream::beg);

        stream.read(reinterpret_cast<char *>(in_buffer.data()), in_buffer.size());
        return(!stream.bad());
    }


    //
    // write_bytes
    //

    bool write_bytes(std::vector<uint8_t> const &in_buffer, std::string const &in_fileName) {
        std::ofstream  stream(in_fileName.c_str(), std::ifstream::binary);
        if (!stream.is_open()) {
            return(false);
        }

        stream.write(reinterpret_cast<char const *>(in_buffer.data()), in_buffer.size());
        return(!stream.bad());
    }


    // ======================================================================================
    //
    //  TokenType
    //
    // ======================================================================================
    enum class TokenType {
        Key,
        Equals,
        Value,
        OpeningBracket,
        ClosingBracket,
        OpeningParenthese,
        ClosingParenthese
    };


    // ======================================================================================
    //
    //  Token
    //
    // ======================================================================================
    struct Token {
        TokenType  type;
        uint32_t   start;  // absolute positions inside the byte array of the config (both inclusive)
        uint32_t   end;    // ditto
    };


    // ======================================================================================
    //
    //  TokenPoolChunk
    //
    // ======================================================================================
    struct TokenPoolChunk {
        Token  tokens[TokenPoolChunkSize];
    };
    

    // ======================================================================================
    //
    //  TokenPool
    //
    // ======================================================================================
    struct TokenPool {
        std::vector<TokenPoolChunk>  chunks;
        uint32_t                     numTokens = 0;
    };


    // ======================================================================================
    //
    //  ConfigEntryType
    //
    // ======================================================================================
    enum class ConfigEntryType {
        Object,
        //List,
        Value
    };


    // ======================================================================================
    //
    //  ConfigString
    //
    // ======================================================================================
    struct ConfigString {
        uint32_t  start;  // absolute positions inside the byte array of the config (both inclusive)
        uint32_t  end;    // ditto
    };


    // ======================================================================================
    //
    //  ConfigEntry
    //
    // ======================================================================================
    struct ConfigEntry {
        ConfigEntryType  type;
        ConfigString     keyString;
        ConfigString     valueString;
        uint32_t         nextEntry;    // this is the next entry after an object or list (it's used to skip whole objects and lists)
    };


    // ======================================================================================
    //
    //  ConfigEntryPoolChunk
    //
    // ======================================================================================
    struct ConfigEntryPoolChunk {
        ConfigEntry  entries[ConfigEntryPoolChunkSize];
    };


    // ======================================================================================
    //
    //  ConfigEntryPool
    //
    // ======================================================================================
    struct ConfigEntryPool {
        std::vector<ConfigEntryPoolChunk>  chunks;
        uint32_t                           numEntries = 0;
    };


    // ======================================================================================
    //
    //  Config
    //
    // ======================================================================================
    struct Config {
        std::vector<uint8_t>  data;
        ConfigEntryPool       entryPool;
    };


    // ======================================================================================
    //
    //  ConfigWalker
    //
    // ======================================================================================
    struct ConfigWalker {
        virtual void begin_object(std::string const &in_name) = 0;
        virtual void key_value(std::string const &in_key, std::string const &in_value) = 0;
        virtual void end_object(std::string const &in_name) = 0;
    };


    //
    // is_newline
    //

    static bool is_newline(uint8_t const in_ch) {
        return(10 == in_ch);
    }


    //
    // is_special_character
    //

    static bool is_special_character(uint8_t const in_ch) {
        return('=' == in_ch || '{' == in_ch || '}' == in_ch || '(' == in_ch || ')' == in_ch);
    }


    //
    // allocate_token
    //

    static void allocate_token(TokenPool *in_pool, TokenType const in_type, uint32_t const in_start, uint32_t const in_end) {
        auto        num = in_pool->numTokens++;
        auto const  index = num / TokenPoolChunkSize;
        if (index >= in_pool->chunks.size()) {
            in_pool->chunks.push_back(TokenPoolChunk());
        }
        auto  token = &in_pool->chunks[index].tokens[num % TokenPoolChunkSize];
        token->type = in_type;
        token->start = in_start;
        token->end = in_end;
    }


    //
    // tokenize_config
    //

    static bool tokenize_config(TokenPool *in_pool, Config const *in_config) {
        uint32_t  pos = 0;

        uint32_t  startPos = 0;
        uint32_t  endPos = 0;

        while (pos < in_config->data.size()) {
            auto  ch = in_config->data[pos++];

            if (is_newline(ch)) {
                continue;  // newline...
            }
            if (std::isspace(ch)) {
                continue;  // whitespace...
            }

            // equals
            if ('=' == ch) {
                allocate_token(in_pool, TokenType::Equals, pos - 1, pos - 1);
                continue;
            }

            // opening/closing bracket
            if ('{' == ch) {
                allocate_token(in_pool, TokenType::OpeningBracket, pos - 1, pos - 1);
                continue;
            }

            if ('}' == ch) {
                allocate_token(in_pool, TokenType::ClosingBracket, pos - 1, pos - 1);
                continue;
            }
            
            // opening/closing parantheses
            // TODO!!

            // value
            if ('"' == ch) {
                startPos = pos;
                while (true) {
                    if (pos >= in_config->data.size()) {
                        return(false);  // end of file inside value...
                    }
                    auto  ch = in_config->data[pos++];
                    if (is_newline(ch)) {
                        return(false);  // newline inside value...
                    }
                    if ('"' == ch) {
                        endPos = pos - 2;
                        break;
                    }
                }
                allocate_token(in_pool, TokenType::Value, startPos, endPos);
                continue;
            }

            // key
            startPos = pos - 1;
            while (true) {
                if (pos >= in_config->data.size()) {
                    return(false);  // end of file inside key...
                }
                auto  ch = in_config->data[pos++];
                if (is_special_character(ch)) {
                    pos -= 1;
                    endPos = pos - 1;
                    break;
                }
                if (std::isspace(ch)) {
                    endPos = pos - 2;
                    break;
                }
            }
            allocate_token(in_pool, TokenType::Key, startPos, endPos);
        }

        return(true);
    }


    //
    // get_token
    //

    static Token const *get_token(TokenPool const *in_pool, uint32_t const in_pos) {
        return(&in_pool->chunks[in_pos / TokenPoolChunkSize].tokens[in_pos % TokenPoolChunkSize]);
    }


    //
    // allocate_entry
    //

    static ConfigEntry *allocate_entry(ConfigEntryPool *in_pool, ConfigEntryType const in_type) {
        auto        num = in_pool->numEntries++;
        auto const  index = num / ConfigEntryPoolChunkSize;
        if (index >= in_pool->chunks.size()) {
            in_pool->chunks.push_back(ConfigEntryPoolChunk());
        }
        auto  entry = &in_pool->chunks[index].entries[num % ConfigEntryPoolChunkSize];
        entry->type = in_type;
        return(entry);
    }

    
    //
    // parse_object
    //

    static uint32_t parse_object(Config *in_config, TokenPool const *in_tokenPool, ConfigEntry *in_objectEntry, uint32_t const in_startToken) {
        uint32_t  tokenPos = in_startToken;

        while (tokenPos < in_tokenPool->numTokens) {
            auto  token = get_token(in_tokenPool, tokenPos);

            // check if the object is finished
            if (TokenType::ClosingBracket == token->type) {
                in_objectEntry->nextEntry = in_config->entryPool.numEntries;
                return(tokenPos + 1);
            }

            // we need at least three tokens (key, equals and value)
            if (tokenPos >= in_tokenPool->numTokens - 2) {
                return(uint32_t(~0));
            }

            // the first token must be a key
            auto  keyToken = get_token(in_tokenPool, tokenPos++);
            if (TokenType::Key != keyToken->type) {
                return(uint32_t(~0));
            }

            // the second token must be an equals
            auto  equalsToken = get_token(in_tokenPool, tokenPos++);
            if (TokenType::Equals != equalsToken->type) {
                return(uint32_t(~0));
            }

            // if we have a value, add it
            token = get_token(in_tokenPool, tokenPos++);
            if (TokenType::Value == token->type) {
                auto  entry = allocate_entry(&in_config->entryPool, ConfigEntryType::Value);
                entry->keyString.start = keyToken->start;
                entry->keyString.end = keyToken->end;
                entry->valueString.start = token->start;
                entry->valueString.end = token->end;
                continue;
            }

            // if we have an object, recurse
            if (TokenType::OpeningBracket == token->type) {
                auto  entry = allocate_entry(&in_config->entryPool, ConfigEntryType::Object);
                entry->keyString.start = keyToken->start;
                entry->keyString.end = keyToken->end;
                tokenPos = parse_object(in_config, in_tokenPool, entry, tokenPos);
            }
        }

        return(uint32_t(~0));
    }
    

    //
    // parse_list
    //




    //
    // parse_entry
    //

    static uint32_t parse_entry(Config *in_config, TokenPool const *in_tokenPool, Token const *in_keyToken, uint32_t const in_startToken) {
        auto  token = get_token(in_tokenPool, in_startToken);

        // if we have a value, we're done
        if (TokenType::Value == token->type) {
            auto  entry = allocate_entry(&in_config->entryPool, ConfigEntryType::Value);
            entry->keyString.start = in_keyToken->start;
            entry->keyString.end = in_keyToken->end;
            entry->valueString.start = token->start;
            entry->valueString.end = token->end;
            return(in_startToken + 1);
        }

        // check for object
        if (TokenType::OpeningBracket == token->type) {
            auto  entry = allocate_entry(&in_config->entryPool, ConfigEntryType::Object);
            entry->keyString.start = in_keyToken->start;
            entry->keyString.end = in_keyToken->end;
            return(parse_object(in_config, in_tokenPool, entry, in_startToken + 1));
        }

        // must be an error
        return(uint32_t(~0));
    }


    //
    // get_entry
    //

    static ConfigEntry const *get_entry(ConfigEntryPool const *in_pool, uint32_t const in_pos) {
        return(&in_pool->chunks[in_pos / ConfigEntryPoolChunkSize].entries[in_pos % ConfigEntryPoolChunkSize]);
    }


    //
    // parse_config
    //

    static bool parse_config(Config *in_config) {
        // tokenize the config
        TokenPool  tokenPool;
        tokenPool.chunks.reserve(16);
        if (!tokenize_config(&tokenPool, in_config)) {
            return(false);
        }
        
        // parse all entries
        uint32_t  tokenPos = 0;
        in_config->entryPool.chunks.reserve(16);
        while (tokenPos < tokenPool.numTokens - 2) {
            auto  keyToken = get_token(&tokenPool, tokenPos);
            if (TokenType::Key != keyToken->type) {
                return(false);
            }
            auto  equalsToken = get_token(&tokenPool, tokenPos + 1);
            if (TokenType::Equals != equalsToken->type) {
                return(false);
            }
            tokenPos = parse_entry(in_config, &tokenPool, keyToken, tokenPos + 2);
        }
        if (uint32_t(~0) == tokenPos) {
            return(false);
        }
        return(true);
    }


    //
    // compare_key
    //

    static bool compare_key(Config const *in_config, ConfigEntry const *in_entry, std::string const &in_key) {
        auto const  len = (in_entry->keyString.end - in_entry->keyString.start) + 1;
        if (len != in_key.length()) {
            return(false);
        }
        for (uint32_t i = 0; i < len; ++i) {
            if (in_config->data[in_entry->keyString.start + i] != in_key[i]) {
                return(false);
            }
        }
        return(true);
    }


    //
    // find_object
    //

    static ConfigEntry const *find_object(Config const *in_config, std::string const &in_key, uint32_t &in_start, uint32_t const in_end) {
        for (uint32_t pos = in_start; pos < in_end; ++pos) {
            auto  entry = get_entry(&in_config->entryPool, pos);
            if (ConfigEntryType::Object != entry->type) {
                continue;
            }
            if (!compare_key(in_config, entry, in_key)) {
                pos = entry->nextEntry - 1;
                continue;
            }
            in_start = pos + 1;
            return(entry);
        }
        return(nullptr);
    }


    //
    // find_value
    //

    static ConfigEntry const *find_value(Config const *in_config, std::string const &in_key, uint32_t const in_start, uint32_t const in_end) {
        for (uint32_t pos = in_start; pos < in_end; ++pos) {
            auto  entry = get_entry(&in_config->entryPool, pos);
            if (ConfigEntryType::Object == entry->type) {
                pos = entry->nextEntry - 1;
                continue;
            }
            if (ConfigEntryType::Value != entry->type) {
                continue;
            }
            if (!compare_key(in_config, entry, in_key)) {
                continue;
            }
            return(entry);
        }
        return(nullptr);
    }


    //
    // config_value
    //

    std::string config_value(Config const *in_config, std::string const &in_key) {
        uint32_t           start = 0;
        uint32_t           end = in_config->entryPool.numEntries;
        std::string        key = in_key;
        ConfigEntry const  *foundEntry = nullptr;

        while (true) {
            auto const  pos = key.find('/');
            if (std::string::npos == pos) {
                foundEntry = find_value(in_config, key, start, end);
                break;
            }
            auto const  subKey = key.substr(0, pos);
            auto        entry = find_object(in_config, subKey, start, end);
            if (nullptr == entry) {
                return("");
            }
            end = entry->nextEntry;
            key = key.substr(pos + 1);
        }

        if (nullptr == foundEntry) {
            return("");
        }
        auto const  len = (foundEntry->valueString.end - foundEntry->valueString.start) + 1;
        return(std::string((char const *)&in_config->data[foundEntry->valueString.start], len));
    }


    //
    // config_from_file
    //

    Config const *config_from_file(std::string const &in_fileName) {
        auto  config = new Config();
        if (!read_bytes(config->data, in_fileName)) {
            delete config;
            return(nullptr);
        }
        if (!parse_config(config)) {
            delete config;
            return(nullptr);
        }
        return(config);
    }


    //
    // config_from_memory
    //

    Config const *config_from_memory(std::vector<uint8_t> const &in_data) {
        auto  config = new Config();
        config->data = in_data;
        if (!parse_config(config)) {
            delete config;
            return(nullptr);
        }
        return(config);
    }


    void test_config() {
        auto  config = config_from_file("engine.oos-config");
        /*
        printf("%s\n", config_value(config, "engine.render-device").c_str());
        printf("%s\n", config_value(config, "engine.window.title").c_str());
        printf("%s\n", config_value(config, "engine2").c_str());
        */
        printf("%s\n", config_value(config, "engine/window/size").c_str());
        printf("%s\n", config_value(config, "engine2").c_str());
        printf("%s\n", config_value(config, "engine3").c_str());

        delete config;
    }
}