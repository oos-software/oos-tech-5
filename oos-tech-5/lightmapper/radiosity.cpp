/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <picture.hpp>
#include <math/math.hpp>
#include <geometry/winding.hpp>
#include <geometry/texture.hpp>
#include <geometry/brush.hpp>
#include <lightmapper/patchmap.hpp>


namespace oos {
    double constexpr  MaxGatherDistance = 2048.0;
    double constexpr  MaxGatherDistanceSquared = MaxGatherDistance * MaxGatherDistance;


    //
    // area_of_triangle
    //

    static double area_of_triangle(math::Vector3 const &in_point0, math::Vector3 const &in_point1, math::Vector3 const &in_point2) {
        return(0.5 * math::cross(in_point0 - in_point1, in_point2 - in_point1).magnitude());
    }


    //
    // calculate_visibility_for_patch
    //

    static double calculate_visibility_for_patch(std::vector<geometry::Brush> const &in_brushes, math::Vector3 const &in_normal, Patch const &in_startPatch, Patch const &in_endPatch) {  // TODO: super sampling
        auto const  direction = in_endPatch.position - in_startPatch.position;
        auto        magnitude = direction.squared_magnitude();
        
        if (magnitude >= MaxGatherDistanceSquared) {
            return(0.0);
        }
        
        magnitude = sqrt(magnitude);
        auto const  ray = math::Ray(in_startPatch.position, direction / magnitude);
        auto const  dot = math::dot(in_normal, ray.direction());
        if (dot < 0.001) {
            return(0.0);
        }
        
        for (auto const &brush : in_brushes) {
            auto const  dist = geometry::intersect_ray_with_brush(brush, ray, 0.005);
            if (dist >= 0.0 && dist <= magnitude) {
                return(0.0);
            }
        }
        
        math::Vector3  points[4];
        auto const     plane = math::Plane(in_normal, in_startPatch.position);
        for (int i = 0; i < 4; ++i) {
            points[i] = (in_endPatch.corners[i] - in_startPatch.position).normalized();
            //points[i] = points[i] - in_normal * math::dot(in_normal, points[i]);
        }
        auto const  totalSurfaceArea = math::PI;//*2.0 **/ math::PI;  // surface area of hemisphere
        //auto const  surfaceArea = math::cross(points[0] - points[1], points[2] - points[1]).magnitude();
        auto const  area0 = area_of_triangle(points[0], points[1], points[2]);
        auto const  area1 = area_of_triangle(points[2], points[3], points[0]);
        return((area0 + area1) / totalSurfaceArea * dot);// ** / (1.0 - magnitude / MaxGatherDistance) * dot);
    }


    //
    // calculate_visibility_for_patch_map
    //

    static void calculate_visibility_for_patch_map(std::vector<geometry::Brush> const &in_brushes, std::vector<PatchMap> &in_patchMaps, size_t const in_index) {
        auto  &patchMap = in_patchMaps[in_index];

        for (auto &startPatch : patchMap.patches) {
            if (!is_patch_valid(startPatch)) {
                continue;
            }

            auto  other = in_patchMaps.data();
            for (size_t index = 0; index < in_patchMaps.size(); ++index, ++other) {
                if (in_index == index) {
                    continue;
                }

                for (auto const &endPatch : other->patches) {
                    if (!is_patch_valid(endPatch)) {
                        continue;
                    }

                    auto const  vis = calculate_visibility_for_patch(in_brushes, patchMap.normal, startPatch, endPatch);
                    if (vis > 0.0) {
                        startPatch.visibility.push_back({ &endPatch, vis });
                    }
                }
            }

            if (!startPatch.visibility.empty()) {
                double  total = 0.0;
                for (auto const &vis : startPatch.visibility) {
                    total += vis.weight;
                }
                //printf("total = %lf\n", total);
                /*total /= double(startPatch.visibility.size());
                for (auto &vis : startPatch.visibility) {
                    vis.weight *= total;
                }*/
            }
        }
    }


    //
    // gather_radiosity_for_patch_map
    //

    static void gather_radiosity_for_patch_map(PatchMap &in_patchMap) {
        for (auto &patch : in_patchMap.patches) {
            for (auto const &vis : patch.visibility) {
                patch.tempRadiosity = patch.tempRadiosity + vis.patch->radiosity * vis.weight;
            }
        }
    }


    //
    // gather_radiosity
    //

    void gather_radiosity(std::vector<geometry::Brush> const &in_brushes, std::vector<PatchMap> &in_patchMaps, int const in_bounce) {
        if (0 == in_bounce) {
            for (size_t index = 0; index < in_patchMaps.size(); ++index) {
                calculate_visibility_for_patch_map(in_brushes, in_patchMaps, index);
            }
        }

        for (auto &pm : in_patchMaps) {
            gather_radiosity_for_patch_map(pm);
        }

        for (auto &pm : in_patchMaps) {
            for (auto &patch : pm.patches) {
                if (!is_patch_valid(patch)) {
                    continue;
                }
                patch.illumination = patch.directIllumination + patch.tempRadiosity;
                patch.radiosity = patch.illumination * patch.reflectivity;
                patch.tempRadiosity = math::Vector3(0.0, 0.0, 0.0);
            }
        }
    }
}