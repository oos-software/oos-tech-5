/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <string>

#include <limits.hpp>
#include <math/vector.hpp>
#include <world/world.hpp>
#include <lightmapper/lightmapper.hpp>
#include <lightmapper/patchmap.hpp>


namespace oos {
    void gather_radiosity(std::vector<geometry::Brush> const &in_brushes, std::vector<PatchMap> &in_patchMaps, int const in_bounce);


    // ======================================================================================
    //
    //  LightmapperShadowFilter
    //
    // ======================================================================================
    enum class LightmapperShadowFilter {
        None,
        PCF_3,
        PCF_5,
        PCF_7,
    };


    // ======================================================================================
    //
    //  LightmapperSettings
    //
    // ======================================================================================
    struct LightmapperSettings {
        int                      numBounces;
        int                      numSuperSamples;
        double                   gatherDistance;
        LightmapperShadowFilter  shadowFilter;
    };


    // ======================================================================================
    //
    //  LightmapperData
    //
    // ======================================================================================
    struct LightmapperData {
        std::vector<geometry::Brush>  brushes;
        std::vector<PatchMap>         patchMaps;
    };


    //
    // lightmapper_settings_for_quality
    //

    static LightmapperSettings lightmapper_settings_for_quality(LightmapperQuality const in_quality) {
        LightmapperSettings  result;

        switch (in_quality) {
        case LightmapperQuality::Low:
            result.numBounces = 0;
            result.numSuperSamples = 0;
            result.gatherDistance = 512.0;
            result.shadowFilter = LightmapperShadowFilter::PCF_3;
            break;

        case LightmapperQuality::Medium:
            result.numBounces = 4;
            result.numSuperSamples = 4;
            result.gatherDistance = 1024.0;
            result.shadowFilter = LightmapperShadowFilter::PCF_3;
            break;

        case LightmapperQuality::High:
            result.numBounces = 16;
            result.numSuperSamples = 16;
            result.gatherDistance = 2048.0;
            result.shadowFilter = LightmapperShadowFilter::PCF_7;
            break;
        }

        return(result);
    }


    //
    // count_patches_in_shadow
    //

    static int count_patches_in_shadow(PatchMap const &in_patchMap, int const in_x, int const in_y, int const in_pcfSize) {
        int   numInShadow = 0;
        auto  center = &in_patchMap.patches[in_y * in_patchMap.width + in_x];

        for (int y = in_y - in_pcfSize; y <= in_y + in_pcfSize; ++y) {
            for (int x = in_x - in_pcfSize; x <= in_x + in_pcfSize; ++x) {
                if (x >= 0 && x < in_patchMap.width && y >= 0 && y < in_patchMap.height) {
                    auto  patch = &in_patchMap.patches[y * in_patchMap.width + x];
                    if (is_patch_valid(*patch)) {
                        if (!patch->hasSunlight) {
                            ++numInShadow;
                        }
                        continue;
                    }
                }
                if (!center->hasSunlight) {
                    ++numInShadow;
                }
            }
        }

        return(numInShadow);
    }


    //
    // gather_directional_lights_for_patch_map
    //

    static void gather_directional_lights_for_patch_map(World const *in_world, LightmapperSettings const &in_settings, PatchMap &in_patchMap) {  // TODO: this only supports one directional light at the moment
       /*auto  dest = in_patchMap.patches.data();

        // calculate the shadow mask for all patches
        for (int y = 0; y < in_patchMap.height; ++y) {
            for (int x = 0; x < in_patchMap.width; ++x, ++dest) {
                if (!is_patch_valid(*dest)) {
                    continue;
                }

                for (auto const &light : in_world->get_directional_lights()) {
                    if (math::dot(light.direction, in_patchMap.normal) > -0.001) {
                        continue;
                    }

                    bool        brushHit = false;
                    auto const  ray = math::Ray(dest->position, light.direction.negated());

                    for (auto const &brush : in_world->get_built_brushes()) {
                        if (geometry::intersect_ray_with_brush(brush, ray, 0.005) >= 0.0) {
                            brushHit = true;
                            break;
                        }
                    }

                    if (!brushHit) {
                        dest->hasSunlight = true;
                    }
                }
            }
        }

        // determine our PCF filter size
        int  pcfSize = 0;

        switch (in_settings.shadowFilter) {
        case LightmapperShadowFilter::PCF_3:
            pcfSize = 1;
            break;

        case LightmapperShadowFilter::PCF_5:
            pcfSize = 2;
            break;

        case LightmapperShadowFilter::PCF_7:
            pcfSize = 3;
            break;
        }

        // calculate the factor for the light based on the surrounding shadow values
        dest = in_patchMap.patches.data();

        if (0 == pcfSize) {
            for (int y = 0; y < in_patchMap.height; ++y) {
                for (int x = 0; x < in_patchMap.width; ++x, ++dest) {
                    if (!is_patch_valid(*dest)) {
                        continue;
                    }
                    if (dest->hasSunlight) {
                        dest->directIllumination = dest->illumination = in_world->get_directional_lights().front().color;
                        dest->radiosity = dest->illumination * dest->reflectivity;
                    }
                }
            }
        }
        else {
            auto const  numSamples = double((pcfSize * 2 + 1) * (pcfSize * 2 + 1));

            for (int y = 0; y < in_patchMap.height; ++y) {
                for (int x = 0; x < in_patchMap.width; ++x, ++dest) {
                    if (!is_patch_valid(*dest)) {
                        continue;
                    }
                    auto const  numInShadow = count_patches_in_shadow(in_patchMap, x, y, pcfSize);
                    auto const  shadowFactor = 1.0 - (numInShadow / numSamples);
                    if (shadowFactor > 0.0) {
                        dest->directIllumination = dest->illumination = in_world->get_directional_lights().front().color * shadowFactor;
                        dest->radiosity = dest->illumination * dest->reflectivity;
                    }
                }
            }
        }*/
    }


    //
    // gather_directional_lights
    //

    static void gather_directional_lights(World const *in_world, LightmapperSettings const &in_settings, LightmapperData &in_data) {/*
        if (in_world->get_directional_lights().empty()) {
            return;
        }
        for (auto &pm : in_data.patchMaps) {
            gather_directional_lights_for_patch_map(in_world, in_settings, pm);
        }*/
    }


    //
    // rebuild_world_lighting
    //

    void rebuild_world_lighting(World *in_world, LightmapperQuality const in_quality) {/*
        LightmapperData  data;
        auto const       settings = lightmapper_settings_for_quality(in_quality);

        // convert the brushes
        data.brushes = std::vector<geometry::Brush>(in_world->get_built_brushes().begin(), in_world->get_built_brushes().end());

        // create all patch maps
        uint32_t  index = 1;
        data.patchMaps.reserve(in_world->get_brush_faces().size());
        for (auto &face : in_world->get_brush_faces()) {
            face.lightMap = index++;
            data.patchMaps.push_back(patch_map_from_face(face, data.brushes, 16.0, 1));
        }

        // gather all directional lights
        gather_directional_lights(in_world, settings, data);

        // perform the radiosity bounces
        for (int bounce = 0; bounce < settings.numBounces; ++bounce) {
            printf("radiosity bounce %d...\n", bounce + 1);
            gather_radiosity(data.brushes, data.patchMaps, bounce);
        }

        // perform dilatation
        for (auto &pm : data.patchMaps) {
            perform_dilatation(pm);
        }

        // write all patch maps (for debugging)
        index = 1;
        for (auto const &pm : data.patchMaps) {
            write_patch_map_png(pm, "lightmaps/" + std::to_string(index++) + ".png");
        }

        // set the light maps for the world
        std::vector<WorldLightMap>  lightMaps;
        lightMaps.reserve(data.patchMaps.size());
        for (auto const &pm : data.patchMaps) {
            WorldLightMap  lm;
            lm.width = pm.width;
            lm.height = pm.height;
            lm.luxels.resize(pm.width * pm.height * 3);
            size_t  offset = 0;
            for (auto const &patch : pm.patches) {
                lm.luxels[offset++] = float(patch.illumination.x());
                lm.luxels[offset++] = float(patch.illumination.y());
                lm.luxels[offset++] = float(patch.illumination.z());
                //if (!is_patch_valid(patch)) {
                //    lm.luxels[offset - 3] = 1.0f;
                //    lm.luxels[offset - 2] = 0.0f;
                //    lm.luxels[offset - 1] = 0.0f;
                //}
            }
            lightMaps.push_back(lm);
        }
        in_world->set_light_maps(lightMaps);*/
    }
}