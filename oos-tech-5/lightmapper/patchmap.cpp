/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <picture.hpp>
#include <math/math.hpp>
#include <geometry/winding.hpp>
#include <geometry/texture.hpp>
#include <geometry/brush.hpp>
#include <lightmapper/patchmap.hpp>


namespace oos {
    int constexpr  MinPatchMapSize = 1;
    int constexpr  MaxPatchMapSize = 4096;


    //
    // is_patch_valid
    //

    bool is_patch_valid(Patch const &in_patch) {
        return(in_patch.isInsideFace && !in_patch.isInsideBrush);
    }


    //
    // project_onto_face_plane
    //

    static math::Vector3 project_onto_face_plane(math::Plane const &in_facePlane, math::Vector3 const &in_normal, math::Vector3 const &in_point, double const in_offset) {
        auto const  dot = math::dot(in_facePlane.normal(), in_normal);
        auto const  dist = in_facePlane.distance_from(in_point) / -dot;
        return(in_point + in_normal * dist + in_normal * in_offset);
    }


    //
    // uv_to_world
    //

    static math::Vector3 uv_to_world(math::Vector3 const &in_origin, math::Vector3 const &in_uAxis, math::Vector3 const &in_vAxis, int const in_u, int const in_v, double const in_resolution) {
        auto const  u = (in_u + 0.5) * in_resolution;  // + 0.5 'cause we want lumel centers
        auto const  v = (in_v + 0.5) * in_resolution;
        return(in_origin + in_uAxis * u + in_vAxis * v);
    }


    //
    // patch_map_from_face
    //
    // this also calculates st for the face
    //

    PatchMap patch_map_from_face(Face &in_face, std::vector<geometry::Brush> const &in_brushes, double const in_resolution, int const in_border) {
        PatchMap    result;
        size_t      minIndex = 0;
        auto const  inf = std::numeric_limits<double>::infinity();
        auto        minU = inf, minV = inf;
        auto        maxU = -inf, maxV = -inf;
        auto const  axes = geometry::texture_axes_for_normal(in_face.plane.normal());
        auto const  normal = math::cross(axes.second, axes.first).normalized();
        auto const  halfRes = in_resolution * 0.5;

        // project all points onto the (axial) plane and find the minimum/maximum point
        for (size_t i = 0; i < in_face.vertexes.size(); ++i) {
            auto const  &point = in_face.vertexes[i].position;
            auto const  u = math::dot(point, axes.first);
            auto const  v = math::dot(point, axes.second);
            if (u < minU) {
                minU = u;
                minIndex = i;
            }
            minV = std::min(minV, v);
            maxU = std::max(maxU, u);
            maxV = std::max(maxV, v);
        }

        // snap the uv projection
        minU = std::floor(minU / in_resolution) * in_resolution;
        minV = std::floor(minV / in_resolution) * in_resolution;
        maxU = std::ceil(maxU / in_resolution) * in_resolution;
        maxV = std::ceil(maxV / in_resolution) * in_resolution;

        // find the origin of the patches
        auto const  &minPoint = in_face.vertexes[minIndex].position;
        auto const  u = math::dot(minPoint, axes.first);
        auto const  v = math::dot(minPoint, axes.second);
        auto        origin = minPoint + axes.first * (minU - u) + axes.second * (minV - v);
        origin = origin - axes.first * (in_border * in_resolution) - axes.second * (in_border * in_resolution);

        // find the size of the patch map
        result.width = uint16_t(math::clamp(int(std::ceil(((maxU - minU) + in_border * in_resolution * 2.0) / in_resolution)), MinPatchMapSize, MaxPatchMapSize));
        result.height = uint16_t(math::clamp(int(std::ceil(((maxV - minV) + in_border * in_resolution * 2.0) / in_resolution)), MinPatchMapSize, MaxPatchMapSize));

        // initialize the patches
        result.patches.resize(result.width * result.height);
        auto  dest = result.patches.data();
        for (int v = 0; v < result.height; ++v) {
            for (int u = 0; u < result.width; ++u, ++dest) {
                dest->position = uv_to_world(origin, axes.first, axes.second, u, v, in_resolution);
                dest->corners[0] = dest->position - axes.first * halfRes - axes.second * halfRes;
                dest->corners[1] = dest->position + axes.first * halfRes - axes.second * halfRes;
                dest->corners[2] = dest->position + axes.first * halfRes + axes.second * halfRes;
                dest->corners[3] = dest->position - axes.first * halfRes + axes.second * halfRes;
                dest->position = project_onto_face_plane(in_face.plane, normal, dest->position, 0.01);
                for (int i = 0; i < 4; ++i) {
                    dest->corners[i] = project_onto_face_plane(in_face.plane, normal, dest->corners[i], 0.01);
                }
                dest->isInsideFace = is_point_inside_face(in_face, dest->position, -0.01);
                dest->isInsideBrush = false;
                for (auto const &brush : in_brushes) {
                    dest->isInsideBrush = geometry::is_point_inside_brush(brush, dest->position, 0.005);
                    if (dest->isInsideBrush) {
                        break;
                    }
                }
                //if (material()->info.isEmissive) {
                    //dest->emittance = dest->directEmittance = m_reflectivity;
                //}
                dest->hasSunlight = false;
                dest->reflectivity = math::Vector3(1.0, 1.0, 1.0);  // TODO!
            }
        }

        // calculate st for the face
        auto const  scaleX = 1.0 / (result.width * in_resolution);
        auto const  scaleY = 1.0 / (result.height * in_resolution);
        for (auto &v : in_face.vertexes) {
            auto const  local = v.position - origin;
            auto const  s = math::dot(local, axes.first)  * scaleX;
            auto const  t = math::dot(local, axes.second) * scaleY;
            v.lightMap = math::Vector2(math::clamp(s, 0.0, 1.0), math::clamp(t, 0.0, 1.0));
        }

        // return our final patch map
        result.normal = in_face.plane.normal();
        return(result);
    }


    //
    // perform_dilatation
    //

    void perform_dilatation(PatchMap &in_patchMap) {
        std::list<int>  toSetInside;

        while (true) {
            auto  patch = in_patchMap.patches.data();

            for (int y = 0; y < in_patchMap.height; ++y) {
                for (int x = 0; x < in_patchMap.width; ++x, ++patch) {
                    if (is_patch_valid(*patch)) {
                        continue;
                    }

                    // search all 9 neighbors
                    math::Vector3  average;
                    int            numValid = 0;

                    for (int x2 = -1; x2 <= 1; ++x2) {
                        auto const  x3 = x + x2;
                        if (x3 < 0 || x3 >= in_patchMap.width) {
                            continue;
                        }
                        for (int y2 = -1; y2 <= 1; ++y2) {
                            auto const  y3 = y + y2;
                            if (y3 < 0 || y3 >= in_patchMap.height) {
                                continue;
                            }
                            auto const  *other = &in_patchMap.patches[y3 * in_patchMap.width + x3];
                            if (is_patch_valid(*other)) {
                                ++numValid;
                                average = average + other->illumination;
                            }
                        }
                    }

                    if (numValid > 0) {
                        toSetInside.push_back(y * in_patchMap.width + x);
                        patch->illumination = average / double(numValid);
                    }
                }
            }

            if (toSetInside.empty()) {
                break;
            }

            for (auto const ind : toSetInside) {
                in_patchMap.patches[ind].isInsideFace = true;
                in_patchMap.patches[ind].isInsideBrush = false;
            }
            toSetInside.clear();
        }
    }


    //
    // write_patch_map_png
    //

    bool write_patch_map_png(PatchMap const &in_patchMap, std::string const &in_fileName) {
        std::vector<uint8_t>  pixels(in_patchMap.width * in_patchMap.height * 4);
        auto                  src = in_patchMap.patches.data();
        auto                  dest = pixels.data();

        for (int y = 0; y < in_patchMap.height; ++y) {
            for (int x = 0; x < in_patchMap.width; ++x, ++src, dest += 4) {
                int const  r = math::clamp(int(src->illumination.x() * 255.0), 0, 255);
                int const  g = math::clamp(int(src->illumination.y() * 255.0), 0, 255);
                int const  b = math::clamp(int(src->illumination.z() * 255.0), 0, 255);
                dest[0] = uint8_t(r);
                dest[1] = uint8_t(g);
                dest[2] = uint8_t(b);
                dest[3] = 255;
            }
        }

        auto const  picture = Picture(in_patchMap.width, in_patchMap.height, pixels.data());
        return(picture.write_png(in_fileName));
    }
}