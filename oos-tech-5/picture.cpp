/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <picture.hpp>

#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#define STBI_NO_FAILURE_STRINGS

#include "../third-party/stb/stb_image.h"
#include "../third-party/stb/stb_image_write.h"


namespace oos {
    // ======================================================================================
    //
    //  Picture
    //
    // ======================================================================================

    //
    // constructor
    //

    Picture::Picture(int const in_width, int const in_height, uint8_t const *in_pixels) :
    m_width(in_width),
    m_height(in_height),
    m_pixels(in_pixels, in_pixels + ((in_width * in_height) << 2)) {
        
    }


    //
    // write_png
    //

    bool Picture::write_png(std::string const &in_fileName) const {
        return(1 == stbi_write_png(in_fileName.c_str(), m_width, m_height, 4, m_pixels.data(), m_width << 2));
    }


    //
    // write_jpeg
    //

    bool Picture::write_jpeg(std::string const &in_fileName, int const in_quality) const {
        return(1 == stbi_write_jpg(in_fileName.c_str(), m_width, m_height, 4, m_pixels.data(), in_quality));
    }


    //
    // from_file
    //

    Picture *Picture::from_file(std::string const &in_fileName) {
        int   width, height, comp;
        auto  pixels = stbi_load(in_fileName.c_str(), &width, &height, &comp, 4);
        if (nullptr == pixels) {
            return nullptr;
        }
        auto  picture = new Picture(width, height, pixels);
        stbi_image_free(pixels);
        return(picture);
    }


    //
    // from_memory
    //

    Picture *Picture::from_memory(std::vector<uint8_t> const &in_data) {
        int   width, height, comp;
        auto  pixels = stbi_load_from_memory(in_data.data(), int(in_data.size()), &width, &height, &comp, 4);
        if (nullptr == pixels) {
            return nullptr;
        }
        auto  picture = new Picture(width, height, pixels);
        stbi_image_free(pixels);
        return(picture);
    }
}