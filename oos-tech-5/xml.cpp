/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <fstream>

#include <xml.hpp>

#include "third-party/rapidxml/rapidxml.hpp"


namespace oos {
    // ======================================================================================
    //
    //  XmlAttributeImplementation
    //
    // ======================================================================================
    class XmlAttributeImplementation : public XmlAttribute {
    public:
        XmlAttributeImplementation(rapidxml::xml_attribute<char> const *in_attribute);

        std::string value() const {
            return(m_attribute->value());
        }


    private:
        rapidxml::xml_attribute<char> const  *m_attribute;
    };


    //
    // constructor
    //

    XmlAttributeImplementation::XmlAttributeImplementation(rapidxml::xml_attribute<char> const *in_attribute) :
    m_attribute(in_attribute) {

    }


    // ======================================================================================
    //
    //  XmlNodeImplementation
    //
    // ======================================================================================
    class XmlNodeImplementation : public XmlNode {
    public:
        XmlNodeImplementation(rapidxml::xml_node<char> const *in_node);

        std::string name() const {
            return(m_node->name());
        }

        std::string value() const {
            return(m_node->value());
        }

        std::string attribute(std::string const &in_name) const;
        std::string value(std::string const &in_name) const;
        
        void for_attributes(std::function<void(XmlAttribute const &)> const &a_callback) const;
        void for_nodes(std::function<void(XmlNode const &)> const &in_callback) const;

        void for_nodes(std::string const &in_name, std::function<void(XmlNode const &)> const &in_callback) const;


    private:
        rapidxml::xml_node<char> const  *m_node;
    };


    //
    // constructor
    //

    XmlNodeImplementation::XmlNodeImplementation(rapidxml::xml_node<char> const *in_node) :
    m_node(in_node) {

    }


    //
    // attribute
    //

    std::string XmlNodeImplementation::attribute(std::string const &in_name) const {
        auto  attrib = m_node->first_attribute(in_name.c_str(), in_name.length());
        if (nullptr == attrib) {
            return("");
        }
        return(attrib->value());
    }


    //
    // value
    //

    std::string XmlNodeImplementation::value(std::string const &in_name) const {
        auto  node = m_node->first_node(in_name.c_str(), in_name.length());
        if (nullptr == node) {
            return("");
        }
        return(node->value());
    }


    //
    // for_attributes
    //

    void XmlNodeImplementation::for_attributes(std::function<void(XmlAttribute const &)> const &in_callback) const {
        auto  current = m_node->first_attribute();
        while (nullptr != current) {
            XmlAttributeImplementation  temp(current);
            in_callback(temp);
            current = current->next_attribute();
        }
    }
    

    //
    // for_nodes
    //

    void XmlNodeImplementation::for_nodes(std::function<void(XmlNode const &)> const &in_callback) const {
        auto  current = m_node->first_node();
        while (nullptr != current) {
            XmlNodeImplementation  temp(current);
            in_callback(temp);
            current = current->next_sibling();
        }
    }

    
    //
    // for_nodes
    //

    void XmlNodeImplementation::for_nodes(std::string const &in_name, std::function<void(XmlNode const &)> const &in_callback) const {
        auto  current = m_node->first_node(in_name.c_str(), in_name.length());
        while (nullptr != current) {
            XmlNodeImplementation  temp(current);
            in_callback(temp);
            current = current->next_sibling(in_name.c_str(), in_name.length());
        }
    }
    

    // ======================================================================================
    //
    //  XmlDocumentImplementation
    //
    // ======================================================================================
    class XmlDocumentImplementation : public XmlDocument {
    public:
        XmlDocumentImplementation(std::vector<uint8_t> const &in_data);
        ~XmlDocumentImplementation();

        bool parse();

        XmlNode const *root_node() const {
            return(m_rootNode);
        }


    private:
        rapidxml::xml_document<>     m_document;
        std::vector<char>            m_documentData;
        XmlNodeImplementation const  *m_rootNode;
    };


    //
    // constructor
    //

    XmlDocumentImplementation::XmlDocumentImplementation(std::vector<uint8_t> const &in_data) :
    m_documentData(in_data.begin(), in_data.end()),
    m_rootNode(nullptr) {

    }


    //
    // destructor
    //

    XmlDocumentImplementation::~XmlDocumentImplementation() {
        delete m_rootNode;
    }


    //
    // parse
    //

    bool XmlDocumentImplementation::parse() {
        try {
            // parse it
            m_document.parse<0>(m_documentData.data());
            auto  root = m_document.first_node();
            if (nullptr == root) {
                return(false);
            }

            // allocate our root node
            m_rootNode = new XmlNodeImplementation(root);
            return(true);
        }
        catch (rapidxml::parse_error const &pe) {
            printf("XML parse error = %s\n", pe.what());
            return(false);
        }
    }


    //
    // from_file
    //

    XmlDocument *XmlDocument::from_file(std::string const &in_fileName) {
        // read the document
        std::ifstream  stream(in_fileName.c_str(), std::ifstream::binary);
        if (!stream.is_open()) {
            return(nullptr);
        }

        stream.seekg(0, std::ifstream::end);
        size_t const  size = stream.tellg();
        stream.seekg(0, std::ifstream::beg);

        std::vector<uint8_t>  data(size + 1);
        stream.read((char *)data.data(), size);
        data[size] = '\0';

        // load it from memory
        return(from_memory(data));
    }


    //
    // from_memory
    //
    
    XmlDocument *XmlDocument::from_memory(std::vector<uint8_t> const &in_data) {
        // check for empty data
        if (in_data.empty()) {
            return(nullptr);
        }

        // create and parse the document
        auto  xml = new XmlDocumentImplementation(in_data);
        if (!xml->parse()) {
            delete xml;
            return(nullptr);
        }
        return(xml);
    }
}