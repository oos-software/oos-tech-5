/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <world/world.hpp>
#include <geometry/csg.hpp>
#include <geometry/vertexhash.hpp>
#include <lightmapper/lightmapper.hpp>


namespace oos {
    // ======================================================================================
    //
    //  World
    //
    // ======================================================================================
    
    //
    // constructor
    //
    
    World::World() {

    }


    //
    // destructor
    //

    World::~World() {

    }


    //
    // add_brush
    //

    void World::add_brush(geometry::Brush const &in_brush) {
        m_brushes.push_back(in_brush);
    }

    /*
    //
    // remove_brush
    //

    void World::remove_brush() {

    }


    //
    // update_brush
    //

    void World::update_brush() {

    }*/

    /*
    //
    // add_directional_light
    //

    void World::add_directional_light(DirectionalLight const &in_light) {
        m_directionalLights.push_back(in_light);
    }


    //
    // add_point_light
    //

    void World::add_point_light(PointLight const &in_light) {
        m_pointLights.push_back(in_light);
    }


    //
    // add_spot_light
    //

    void World::add_spot_light(SpotLight const &in_light) {
        m_spotLights.push_back(in_light);
    }
    */

    //
    // rebuild_world_geometry
    //

    static void rebuild_world_geometry(World *in_world) {
        auto const  builtBrushes = geometry::rebuild_brush_geometry(in_world->get_brushes());

        // create windings
        std::list<geometry::Winding>  windings;
        for (auto const &brush : builtBrushes) {
            windings.splice(windings.end(), geometry::create_brush_windings(brush, MaxWorldCoordinate - MinWorldCoordinate));
        }

        // create faces
        std::list<Face>       faces;
        geometry::VertexHash  vertexHash;

        for (auto const &winding : windings) {
            for (auto const &point : winding) {

            }

            faces.push_back(face_from_winding(winding));
        }

        // set our final geometry
        in_world->set_built_brushes(builtBrushes);
        in_world->set_brush_faces(faces);
    }


    //
    // rebuild_world
    //

    void rebuild_world(World *in_world, bool const in_calculateLighting, LightmapperQuality const in_quality) {
        rebuild_world_geometry(in_world);

        if (in_calculateLighting) {
            rebuild_world_lighting(in_world, in_quality);
        }
    }
}