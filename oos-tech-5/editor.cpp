/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <editor.hpp>


namespace oos {
    // ======================================================================================
    //
    //  EditorImportImplementation
    //
    // ======================================================================================
    struct EditorImportImplementation : public EditorImport {
        World *load_world(std::string const &in_name);
        bool save_world(World *in_world, std::string const &in_name);
        void rebuild_world(World *in_world);
        void draw_world(World *in_world);
    };


    //
    // load_world
    //

    World *EditorImportImplementation::load_world(std::string const &in_name) {
        return(nullptr);
    }


    //
    // save_world
    //

    bool EditorImportImplementation::save_world(World *in_world, std::string const &in_name) {
        return(false);
    }


    //
    // rebuild_world
    //

    void EditorImportImplementation::rebuild_world(World *in_world) {

    }


    //
    // draw_world
    //

    void EditorImportImplementation::draw_world(World *in_world) {

    }
    

    //
    // API
    //

    EditorImport *create_editor_import() {
        return(new EditorImportImplementation());
    }

    void destroy_editor_import(EditorImport *in_import) {
        delete static_cast<EditorImportImplementation *>(in_import);
    }
}