/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <cmath>

#include <math/vector.hpp>
#include <math/matrix.hpp>


namespace oos {
    namespace math {
        // ======================================================================================
        //
        //  Vector2
        //
        // ======================================================================================
        Vector2::Vector2() : Vector2(0.0, 0.0) {

        }

        Vector2::Vector2(double const in_x, double const in_y) {
            m_value[0] = in_x;
            m_value[1] = in_y;
        }


        //
        // +
        //

        Vector2 Vector2::operator +(Vector2 const &in_other) const {
            return(Vector2(x() + in_other.x(), y() + in_other.y()));
        }


        //
        // -
        //

        Vector2 Vector2::operator -(Vector2 const &in_other) const {
            return(Vector2(x() - in_other.x(), y() - in_other.y()));
        }


        //
        // *
        //

        Vector2 Vector2::operator *(double const in_scalar) const {
            return(Vector2(x() * in_scalar, y() * in_scalar));
        }


        //
        // /
        //

        Vector2 Vector2::operator /(double const in_scalar) const {
            return(Vector2(x() / in_scalar, y() / in_scalar));
        }


        //
        // * (component multiplication)
        //

        Vector2 Vector2::operator *(Vector2 const &in_other) const {
            return(Vector2(x() * in_other.x(), y() * in_other.y()));
        }


        // ======================================================================================
        //
        //  Vector3
        //
        // ======================================================================================
        Vector3::Vector3() : Vector3(0.0, 0.0, 0.0) {

        }

        Vector3::Vector3(double const in_x, double const in_y, double const in_z) {
            m_value[0] = in_x;
            m_value[1] = in_y;
            m_value[2] = in_z;
        }

        Vector3::Vector3(double const *in_values) {
            m_value[0] = in_values[0];
            m_value[1] = in_values[1];
            m_value[2] = in_values[2];
        }


        //
        // +
        //

        Vector3 Vector3::operator +(Vector3 const &in_other) const {
            return(Vector3(x() + in_other.x(), y() + in_other.y(), z() + in_other.z()));
        }


        //
        // -
        //

        Vector3 Vector3::operator -(Vector3 const &in_other) const {
            return(Vector3(x() - in_other.x(), y() - in_other.y(), z() - in_other.z()));
        }


        //
        // *
        //

        Vector3 Vector3::operator *(double const in_scalar) const {
            return(Vector3(x() * in_scalar, y() * in_scalar, z() * in_scalar));
        }


        //
        // /
        //

        Vector3 Vector3::operator /(double const in_scalar) const {
            return(Vector3(x() / in_scalar, y() / in_scalar, z() / in_scalar));
        }


        //
        // * (component multiplication)
        //

        Vector3 Vector3::operator *(Vector3 const &in_other) const {
            return(Vector3(x() * in_other.x(), y() * in_other.y(), z() * in_other.z()));
        }


        //
        // * (matrix multiplication)
        //

        Vector3 Vector3::operator *(Matrix3 const &in_matrix) const {
            auto const  xr = x() * in_matrix[0] + y() * in_matrix[1] + z() * in_matrix[2];
            auto const  yr = x() * in_matrix[3] + y() * in_matrix[4] + z() * in_matrix[5];
            auto const  zr = x() * in_matrix[6] + y() * in_matrix[7] + z() * in_matrix[8];
            return(Vector3(xr, yr, zr));
        }


        //
        // magnitude
        //

        double Vector3::magnitude() const {
            return(sqrt(squared_magnitude()));
        }


        //
        // squared_magnitude
        //

        double Vector3::squared_magnitude() const {
            return(dot(*this, *this));
        }


        //
        // abs
        //

        Vector3 Vector3::abs() const {
            return(Vector3(std::abs(m_value[0]), std::abs(m_value[1]), std::abs(m_value[2])));
        }


        //
        // negated
        //

        Vector3 Vector3::negated() const {
            return(Vector3(-m_value[0], -m_value[1], -m_value[2]));
        }


        //
        // normalized
        //

        Vector3 Vector3::normalized() const {
            auto const  mag = magnitude();
            return(*this / mag);
        }


        //
        // perpendicular
        //

        Vector3 Vector3::perpendicular() const {
            double     result[3] = { 0 };
            int const  first = largest_index();
            int const  second = second_largest_index(first);
            result[first] = -get(second);
            result[second] = get(first);
            return(Vector3(result[0], result[1], result[2]));
        }


        //
        // replace
        //

        Vector3 Vector3::replace(int const in_index, double const in_value) const {
            double  result[3] = { m_value[0], m_value[1], m_value[2] };
            result[in_index] = in_value;
            return(Vector3(result[0], result[1], result[2]));
        }


        //
        // snap
        //

        Vector3 Vector3::snap(double const in_grid) const {
            auto const  sx = round(m_value[0] / in_grid) * in_grid;
            auto const  sy = round(m_value[1] / in_grid) * in_grid;
            auto const  sz = round(m_value[2] / in_grid) * in_grid;
            return(Vector3(sx, sy, sz));
        }


        // ======================================================================================
        //
        //  Vector4
        //
        // ======================================================================================
        Vector4::Vector4() : Vector4(0.0, 0.0, 0.0, 0.0) {

        }

        Vector4::Vector4(double const in_x, double const in_y, double const in_z, double const in_w) {
            m_value[0] = in_x;
            m_value[1] = in_y;
            m_value[2] = in_z;
            m_value[3] = in_w;
        }

        //
        // +
        //

        Vector4 Vector4::operator +(Vector4 const &in_other) const {
            return(Vector4(x() + in_other.x(), y() + in_other.y(), z() + in_other.z(), w() + in_other.w()));
        }


        //
        // -
        //

        Vector4 Vector4::operator -(Vector4 const &in_other) const {
            return(Vector4(x() - in_other.x(), y() - in_other.y(), z() - in_other.z(), w() - in_other.w()));
        }


        //
        // *
        //

        Vector4 Vector4::operator *(double const in_scalar) const {
            return(Vector4(x() * in_scalar, y() * in_scalar, z() * in_scalar, w() * in_scalar));
        }


        //
        // /
        //

        Vector4 Vector4::operator /(double const in_scalar) const {
            return(Vector4(x() / in_scalar, y() / in_scalar, z() / in_scalar, w() / in_scalar));
        }


        //
        // * (matrix multiplication)
        //

        Vector4 Vector4::operator *(Matrix4 const &in_matrix) const {
            auto const  xr = x() * in_matrix[0] + y() * in_matrix[1] + z() * in_matrix[2] + w() * in_matrix[3];
            auto const  yr = x() * in_matrix[4] + y() * in_matrix[5] + z() * in_matrix[6] + w() * in_matrix[7];
            auto const  zr = x() * in_matrix[8] + y() * in_matrix[9] + z() * in_matrix[10] + w() * in_matrix[11];
            auto const  wr = x() * in_matrix[12] + y() * in_matrix[13] + z() * in_matrix[14] + w() * in_matrix[15];
            return(Vector4(xr, yr, zr, wr));
        }


        //
        // replace
        //

        Vector4 Vector4::replace(int const in_index, double const in_value) const {
            double  result[4] = { m_value[0], m_value[1], m_value[2], m_value[3] };
            result[in_index] = in_value;
            return(Vector4(result[0], result[1], result[2], result[3]));
        }


        //
        // dot
        //

        double dot(Vector2 const &in_vec0, Vector2 const &in_vec1) {
            return(in_vec0.x() * in_vec1.x() + in_vec0.y() * in_vec1.y());
        }

        double dot(Vector3 const &in_vec0, Vector3 const &in_vec1) {
            return(in_vec0.x() * in_vec1.x() + in_vec0.y() * in_vec1.y() + in_vec0.z() * in_vec1.z());
        }

        double dot(Vector4 const &in_vec0, Vector4 const &in_vec1) {
            return(in_vec0.x() * in_vec1.x() + in_vec0.y() * in_vec1.y() + in_vec0.z() * in_vec1.z() + in_vec0.w() * in_vec1.w());
        }


        //
        // cross
        //

        double cross(Vector2 const &in_vec0, Vector2 const &in_vec1) {
            return(in_vec0.x() * in_vec1.y() - in_vec0.y() * in_vec1.x());
        }

        Vector3 cross(Vector3 const &in_vec0, Vector3 const &in_vec1) {
            auto const  x = in_vec0.y() * in_vec1.z() - in_vec0.z() * in_vec1.y();
            auto const  y = in_vec0.z() * in_vec1.x() - in_vec0.x() * in_vec1.z();
            auto const  z = in_vec0.x() * in_vec1.y() - in_vec0.y() * in_vec1.x();
            return(Vector3(x, y, z));
        }
    }
}