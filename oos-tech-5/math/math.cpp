/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <math/math.hpp>
#include <math/vector.hpp>


namespace oos {
    namespace math {
        //
        // to_radians
        //

        double to_radians(double const in_deg) {
            return(in_deg / 180.0 * PI);
        }


        //
        // to_degrees
        //

        double to_degrees(double const in_rad) {
            return(in_rad / PI * 180.0);
        }


        //
        // clamp_angle
        //

        double clamp_angle(double const in_value) {
            double  val = in_value;

            while (val < 0.0) {
                val += 360.0;
            }

            while (val >= 360.0) {
                val -= 360.0;
            }

            return(val);
        }


        //
        // closest_point_on_line_fraction
        //

        double closest_point_on_line_fraction(Vector3 const &in_start, Vector3 const &in_end, Vector3 const &in_point, bool const in_clamp) {
            auto const  u = in_point - in_start;
            auto const  v = in_end - in_start;
            auto const  d = dot(u, v) / v.squared_magnitude();
            return(in_clamp ? clamp(d, 0.0, 1.0) : d);
        }


        //
        // closest_point_on_line
        //

        Vector3 closest_point_on_line(Vector3 const &in_start, Vector3 const &in_end, Vector3 const &in_point, bool const in_clamp) {
            auto const  d = closest_point_on_line_fraction(in_start, in_end, in_point, in_clamp);
            return(in_start + (in_end - in_start) * d);
        }
    }
}