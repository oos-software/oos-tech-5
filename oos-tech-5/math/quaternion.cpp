/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <math/quaternion.hpp>


namespace oos {
    namespace math {
        // ======================================================================================
        //
        //  Quaternion
        //
        // ======================================================================================

        //
        // constructors
        //

        Quaternion::Quaternion() :
        Quaternion(0.0, 0.0, 0.0, 1.0) {

        }

        Quaternion::Quaternion(double const in_x, double const in_y, double const in_z, double const in_w) :
        m_x(in_x),
        m_y(in_y),
        m_z(in_z),
        m_w(in_w) {

        }

        Quaternion::Quaternion(Vector3 const &in_point) :
        Quaternion(in_point.x(), in_point.y(), in_point.z(), 0.0) {  // point

        }

        Quaternion::Quaternion(Vector3 const &in_axis, double const in_angle) {  // axis angle
            auto const  a = math::to_radians(in_angle * 0.5);
            auto const  s = sin(a);
            m_x = in_axis.x() * s;
            m_y = in_axis.y() * s;
            m_z = in_axis.z() * s;
            m_w = cos(a);
        }


        //
        // * (concatenation)
        //

        Quaternion Quaternion::operator *(Quaternion const &in_other) const {
            auto        A = (m_w + m_x) * (in_other.m_w + in_other.m_x);
            auto        B = (m_z - m_y) * (in_other.m_y - in_other.m_z);
            auto        C = (m_w - m_x) * (in_other.m_y + in_other.m_z);
            auto        D = (m_y + m_z) * (in_other.m_w - in_other.m_x);
            auto const  E = (m_x + m_z) * (in_other.m_x + in_other.m_y);
            auto const  F = (m_x - m_z) * (in_other.m_x - in_other.m_y);
            auto const  G = (m_w + m_y) * (in_other.m_w - in_other.m_z);
            auto const  H = (m_w - m_y) * (in_other.m_w + in_other.m_z);

            A = A - (E + F + G + H) * 0.5;
            C = C + (E - F + G - H) * 0.5;
            D = D + (E - F - G + H) * 0.5;
            B = B + (-E - F + G + H) * 0.5;

            return(Quaternion(A, C, D, B));
        }


        //
        // * (point rotation)
        //

        Vector3 Quaternion::operator *(Vector3 const &in_point) const {
            auto const  p = Quaternion(in_point);
            auto const  result = *this * p * conjugate();
            return(result.to_point());
        }


        //
        // to_matrix
        //

        Matrix3 Quaternion::to_matrix() const {
            auto const  xx = m_x * m_x;
            auto const  xy = m_x * m_y;
            auto const  xz = m_x * m_z;
            auto const  xw = m_x * m_w;

            auto const  yy = m_y * m_y;
            auto const  yz = m_y * m_z;
            auto const  yw = m_y * m_w;

            auto const  zz = m_z * m_z;
            auto const  zw = m_z * m_w;

            return(Matrix3(
                1.0 - 2.0 * (yy + zz), 2.0 * (xy - zw), 2.0 * (xz + yw),
                2.0 * (xy + zw), 1.0 - 2.0 * (xx + zz), 2.0 * (yz - xw),
                2.0 * (xz - yw), 2.0 * (yz + xw), 1.0 - 2.0 * (xx + yy)));
        }


        //
        // dot
        //

        double dot(Quaternion const &in_quat0, Quaternion const &in_quat1) {
            return(in_quat0.x() * in_quat1.x() + in_quat0.y() * in_quat1.y() + in_quat0.z() * in_quat1.z() + in_quat0.w() * in_quat1.w());
        }

    }
}