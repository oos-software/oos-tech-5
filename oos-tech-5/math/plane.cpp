/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <math/plane.hpp>


namespace oos {
    namespace math {
        // ======================================================================================
        //
        //  Plane
        //
        // ======================================================================================

        //
        // constructors
        //

        Plane::Plane() :
        m_distance(0) {

        }

        Plane::Plane(Vector3 const &in_normal, double const in_distance) :
        m_normal(in_normal),
        m_distance(in_distance) {

        }

        Plane::Plane(Vector3 const &in_normal, Vector3 const &in_point) :
        m_normal(in_normal),
        m_distance(math::dot(in_normal, in_point)) {

        }


        //
        // distance_from
        //

        double Plane::distance_from(Vector3 const &in_point) const {
            return(dot(m_normal, in_point) - m_distance);
        }


        //
        // intersect a ray with the plane
        //
        // returns -1 if there was no intersection
        //

        double Plane::intersect_ray(Ray const &in_ray) const {
            auto const  dist = distance_from(in_ray.origin());

            if (dist > 0.0001) {
                auto const  dot = math::dot(m_normal, in_ray.direction());
                if (dot < -0.0001) {
                    return(dist / -dot);
                }
                return(-1.0);
            }

            if (dist < -0.0001) {
                auto const  dot = math::dot(m_normal, in_ray.direction());
                if (dot > 0.0001) {
                    return(dist / -dot);
                }
                return(-1.0);
            }

            return(0.0);
        }


        //
        // negated
        //

        Plane Plane::negated() const {
            return(Plane(m_normal.negated(), -m_distance));
        }


        //
        // from_points
        //

        Plane Plane::from_points(Vector3 const &in_point0, Vector3 const &in_point1, Vector3 const &in_point2) {
            auto const  dir0 = in_point0 - in_point1;
            auto const  dir1 = in_point2 - in_point1;
            auto const  normal(cross(dir1, dir0).normalized());
            return(Plane(normal, in_point1));
        }
    }
}