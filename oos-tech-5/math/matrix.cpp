/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <cmath>

#include <math/math.hpp>
#include <math/matrix.hpp>


namespace oos {
    namespace math {
        // ======================================================================================
        //
        //  Matrix2
        //
        // ======================================================================================
        Matrix2::Matrix2() : Matrix2(
            1, 0,
            0, 1) {

        }

        Matrix2::Matrix2(double const in_00, double const in_01, double const in_10, double const in_11) {
            m_value[0] = in_00;
            m_value[1] = in_01;

            m_value[2] = in_10;
            m_value[3] = in_11;
        }


        //
        // *
        //

        Matrix2 Matrix2::operator *(Matrix2 const &in_other) const {
            Matrix2  result(0, 0, 0, 0);
            result.multiply(*this, in_other);
            return(result);
        }


        //
        // transposed
        //

        Matrix2 Matrix2::transposed() const {
            return(Matrix2(
                m_value[0], m_value[2],
                m_value[1], m_value[3]));
        }


        // ======================================================================================
        //
        //  Matrix3
        //
        // ======================================================================================
        Matrix3::Matrix3() : Matrix3(
            1, 0, 0,
            0, 1, 0,
            0, 0, 1) {

        }

        Matrix3::Matrix3(
            double const in_00, double const in_01, double const in_02,
            double const in_10, double const in_11, double const in_12,
            double const in_20, double const in_21, double const in_22) {
            m_value[0] = in_00;
            m_value[1] = in_01;
            m_value[2] = in_02;

            m_value[3] = in_10;
            m_value[4] = in_11;
            m_value[5] = in_12;

            m_value[6] = in_20;
            m_value[7] = in_21;
            m_value[8] = in_22;
        }


        //
        // *
        //

        Matrix3 Matrix3::operator *(Matrix3 const &in_other) const {
            Matrix3  result(0, 0, 0, 0, 0, 0, 0, 0, 0);
            result.multiply(*this, in_other);
            return(result);
        }


        //
        // transposed
        //

        Matrix3 Matrix3::transposed() const {
            return(Matrix3(
                m_value[0], m_value[3], m_value[6],
                m_value[1], m_value[4], m_value[7],
                m_value[2], m_value[5], m_value[8]));
        }


        // ======================================================================================
        //
        //  Matrix4
        //
        // ======================================================================================
        Matrix4::Matrix4() : Matrix4(
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1) {

        }

        Matrix4::Matrix4(
            double const in_00, double const in_01, double const in_02, double const in_03,
            double const in_10, double const in_11, double const in_12, double const in_13,
            double const in_20, double const in_21, double const in_22, double const in_23,
            double const in_30, double const in_31, double const in_32, double const in_33) {
            m_value[0] = in_00;
            m_value[1] = in_01;
            m_value[2] = in_02;
            m_value[3] = in_03;

            m_value[4] = in_10;
            m_value[5] = in_11;
            m_value[6] = in_12;
            m_value[7] = in_13;

            m_value[8] = in_20;
            m_value[9] = in_21;
            m_value[10] = in_22;
            m_value[11] = in_23;

            m_value[12] = in_30;
            m_value[13] = in_31;
            m_value[14] = in_32;
            m_value[15] = in_33;
        }


        //
        // *
        //

        Matrix4 Matrix4::operator *(Matrix4 const &in_other) const {
            Matrix4  result(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            result.multiply(*this, in_other);
            return(result);
        }


        //
        // transposed
        //

        Matrix4 Matrix4::transposed() const {
            return(Matrix4(
                m_value[0], m_value[4], m_value[8], m_value[12],
                m_value[1], m_value[5], m_value[9], m_value[13],
                m_value[2], m_value[6], m_value[10], m_value[14],
                m_value[3], m_value[7], m_value[11], m_value[15]));
        }


        //
        // scale_matrix
        //

        Matrix3 scale_matrix(double const in_scale) {
            return(scale_matrix(in_scale, in_scale, in_scale));
        }

        Matrix3 scale_matrix(double const in_xScale, double const in_yScale, double const in_zScale) {
            return(Matrix3(
                in_xScale, 0.0, 0.0,
                0.0, in_yScale, 0.0,
                0.0, 0.0, in_zScale));
        }


        //
        // rotation_matrix
        //
        // right hand rule
        //

        Matrix3 rotation_matrix(int const in_axis, double const in_angle) {
            auto const  a = to_radians(in_angle);
            auto const  s = sin(a), c = cos(a);

            switch (in_axis) {
            case 0:
                return(Matrix3(
                    1, 0, 0,
                    0, c, -s,
                    0, s, c));

            case 1:
                return(Matrix3(
                    c, 0, s,
                    0, 1, 0,
                    -s, 0, c));

            default:
                return(Matrix3(
                    c, -s, 0,
                    s, c, 0,
                    0, 0, 1));
            }
        }


        //
        // translation_matrix
        //

        Matrix4 translation_matrix(math::Vector3 const &in_translation) {
            return(Matrix4(
                1, 0, 0, in_translation.x(),
                0, 1, 0, in_translation.y(),
                0, 0, 1, in_translation.z(),
                0, 0, 0, 1));
        }


        //
        // to_matrix3
        //

        Matrix3 to_matrix3(Matrix4 const &in_matrix) {
            return(Matrix3(
                in_matrix[0], in_matrix[1], in_matrix[2],
                in_matrix[4], in_matrix[5], in_matrix[6],
                in_matrix[8], in_matrix[9], in_matrix[10]));
        }


        //
        // to_matrix4
        //

        Matrix4 to_matrix4(Matrix3 const &in_matrix) {
            return(Matrix4(
                in_matrix[0], in_matrix[1], in_matrix[2], 0,
                in_matrix[3], in_matrix[4], in_matrix[5], 0,
                in_matrix[6], in_matrix[7], in_matrix[8], 0,
                0, 0, 0, 1
            ));
        }
    }
}