/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <math/ray.hpp>


namespace oos {
    namespace math {
        // ======================================================================================
        //
        //  Ray
        //
        // ======================================================================================

        //
        // constructor
        //

        Ray::Ray(Vector3 const &in_origin, Vector3 const &in_direction) :
        m_origin(in_origin),
        m_direction(in_direction) {

        }
    }
}