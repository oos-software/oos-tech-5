/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <framework.hpp>
#include <engine.hpp>

#include "../third-party/s7/s7.h"


namespace oos {
    //
    // static variables
    //

    static Engine     s_engine;
    static Framework  *s_framework;
    static s7_scheme  *s_scheme;


    //
    // error
    //

    void error(std::string const &in_description) {
        printf("FATAL ERROR: %s\n", in_description.c_str());
        system("pause");
        exit(-1);
    }


    //
    // get_scheme
    //

    s7_scheme *get_scheme() {
        return(s_scheme);
    }


    //
    // scheme_print
    //

    static void scheme_print(s7_scheme *sc, uint8_t c, s7_pointer port) {
        fprintf(stderr, "%c", c);
    }


    //
    // create_engine
    //

    Engine *create_engine() {
        // framework
        s_framework = create_framework();
        if (nullptr == s_framework) {
            return(nullptr);
        }

        // window
        s_engine.window = s_framework->create_window(1664, 936, "Engine");  // TODO!!
        if (nullptr == s_engine.window) {
            return(nullptr);
        }

        // system device


        // input device
        s_engine.inputDevice = s_framework->create_input_device(s_engine.window);
        if (nullptr == s_engine.inputDevice) {
            return(nullptr);
        }

        // render device
        s_engine.renderDevice = s_framework->create_render_device(s_engine.window, oos::RenderDeviceType::OpenGL);  // TODO!
        if (nullptr == s_engine.renderDevice) {
            return(nullptr);
        }

        // audio device
        

        // input system
        s_engine.inputSystem = new InputSystem();

        // render system
        s_engine.renderSystem = new RenderSystem();

        // s7 scheme interpreter
        s_scheme = s7_init();
        if (nullptr == s_scheme) {
            return(nullptr);
        }
        s7_set_current_output_port(s_scheme, s7_open_output_function(s_scheme, scheme_print));
        if (nullptr == s7_load(s_scheme, "scripts/project.scm")) {
            return(nullptr);
        }

        // show our window
        s_engine.window->show();
        return(&s_engine);
    }


    //
    // destroy_engine
    //

    void destroy_engine() {
        if (nullptr == s_framework) {
            return;
        }

        // s7 scheme interpreter
        if (nullptr != s_scheme) {
            s7_free(s_scheme);
            s_scheme = nullptr;
        }

        // render system
        delete s_engine.renderSystem;
        s_engine.renderSystem = nullptr;

        // input system
        delete s_engine.inputSystem;
        s_engine.inputSystem = nullptr;

        // audio device

        
        // render device
        s_framework->destroy_render_device(s_engine.renderDevice);
        s_engine.renderDevice = nullptr;

        // input device
        s_framework->destroy_input_device(s_engine.inputDevice);
        s_engine.inputDevice = nullptr;

        // system device
        

        // window
        s_framework->destroy_window(s_engine.window);
        s_engine.window = nullptr;

        // framework
        destroy_framework(s_framework);
        s_framework = nullptr;
    }


    //
    // run_engine
    //

    void run_engine(std::function<void(Engine *)> const &in_callback) {
        while (true) {
            auto const  result = s_framework->update(s_engine.window, [&in_callback]() {
                s_engine.inputSystem->update(s_engine.inputDevice);
                in_callback(&s_engine);
            });
            if (false == result) {
                break;
            }
        }
    }
}