/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <geometry/winding.hpp>


namespace oos {
    namespace geometry {
        //
        // chop_winding
        //
        // behind the plane is kept
        //

        Winding chop_winding(Winding const &in_winding, math::Plane const &in_plane, double const in_epsilon) {
            Winding  result;
            result.reserve(in_winding.size());

            for (size_t i = in_winding.size() - 1, j = 0; j < in_winding.size(); i = j++) {
                auto const  &point0 = in_winding[i];
                auto const  &point1 = in_winding[j];

                auto const  dist0 = in_plane.distance_from(point0);
                auto const  dist1 = in_plane.distance_from(point1);

                auto const  in0 = dist0 <= in_epsilon;
                auto const  in1 = dist1 <= in_epsilon;

                if (!in0 && !in1) {
                    continue;
                }

                bool  add = false;

                if (in0) {
                    result.push_back(point0);
                    if (!in1) {
                        add = true;
                    }
                }
                else {
                    add = true;
                }

                if (add) {
                    auto const  frac = -dist0 / (dist1 - dist0);
                    result.push_back(point0 + (point1 - point0) * frac);
                }
            }

            return(result);
        }


        //
        // winding_from_points
        //

        Winding winding_from_points(math::Vector3 const &in_point0, math::Vector3 const &in_point1, math::Vector3 const &in_point2, double const in_baseSize) {
            auto const  right = (in_point2 - in_point1).normalized() * in_baseSize;
            auto const  up = (in_point0 - in_point1).normalized() * in_baseSize;

            auto const  p0 = in_point1 - right - up;
            auto const  p1 = in_point1 + right - up;
            auto const  p2 = in_point1 + right + up;
            auto const  p3 = in_point1 - right + up;

            return { p0, p1, p2, p3 };
        }


        //
        // plane_from_winding
        //

        math::Plane plane_from_winding(Winding const &in_winding) {
            return(math::Plane::from_points(in_winding[0], in_winding[1], in_winding[2]));
        }
    }
}