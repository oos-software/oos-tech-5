/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <geometry/brush.hpp>


namespace oos {
    namespace geometry {
        static int64_t constexpr  FractionBits = 6;  // 58.6 fixed point
        static double constexpr   FractionScale = 1 << FractionBits;

        
        //
        // brush_position_from_vector
        //

        BrushPosition brush_position_from_vector(math::Vector3 const &in_position) {
            auto const  x = int64_t(in_position.x() * FractionScale);
            auto const  y = int64_t(in_position.y() * FractionScale);
            auto const  z = int64_t(in_position.z() * FractionScale);
            return { x, y, z };
        }


        //
        // vector_from_brush_position
        //

        math::Vector3 vector_from_brush_position(BrushPosition const &in_position) {
            return(math::Vector3(in_position.x / FractionScale, in_position.y / FractionScale, in_position.z / FractionScale));
        }


        //
        // brush_position_from_integer
        //

        BrushPosition brush_position_from_integer(int64_t const in_x, int64_t const in_y, int64_t const in_z) {
            return { in_x << FractionBits, in_y << FractionBits, in_z << FractionBits };
        }


        //
        // + (BrushPosition)
        //

        BrushPosition operator +(BrushPosition const &in_a, BrushPosition const &in_b) {
            return { in_a.x + in_b.x, in_a.y + in_b.y, in_a.z + in_b.z };
        }


        //
        // - (BrushPosition)
        //

        BrushPosition operator -(BrushPosition const &in_a, BrushPosition const &in_b) {
            return { in_a.x - in_b.x, in_a.y - in_b.y, in_a.z - in_b.z };
        }


        //
        // >> (BrushPosition)
        //

        BrushPosition operator >>(BrushPosition const &in_pos, int64_t const in_shift) {
            return { in_pos.x >> in_shift, in_pos.y >> in_shift, in_pos.z >> in_shift };
        }


        //
        // snap_brush_position
        //

        BrushPosition snap_brush_position(BrushPosition const &in_pos, int64_t const in_snap) {
            auto const  hs = in_snap >> 1;
            auto const  x = (in_pos.x < 0 ? in_pos.x - hs : in_pos.x + hs);
            auto const  y = (in_pos.y < 0 ? in_pos.y - hs : in_pos.y + hs);
            auto const  z = (in_pos.z < 0 ? in_pos.z - hs : in_pos.z + hs);
            auto const  xs = (x / in_snap) * in_snap;
            auto const  ys = (y / in_snap) * in_snap;
            auto const  zs = (z / in_snap) * in_snap;
            return { xs, ys, zs };
        }


        //
        // plane_from_brush_side
        //

        math::Plane plane_from_brush_side(BrushSide const &in_side) {
            auto const  point0 = vector_from_brush_position(in_side.position[0]);
            auto const  point1 = vector_from_brush_position(in_side.position[1]);
            auto const  point2 = vector_from_brush_position(in_side.position[2]);
            return(math::Plane::from_points(point0, point1, point2));
        }


        //
        // create_brush_windings
        //

        std::list<Winding> create_brush_windings(Brush const &in_brush, double const in_baseSize) {
            std::list<Winding>        result;
            std::vector<math::Plane>  planes(in_brush.sides.size());

            // create all planes
            for (int i = 0; i < in_brush.sides.size(); ++i) {
                planes[i] = plane_from_brush_side(in_brush.sides[i]);
            }

            // create all windings
            for (int i = 0; i < in_brush.sides.size(); ++i) {
                // create the base winding
                auto const  &side = in_brush.sides[i];
                auto const  point0 = vector_from_brush_position(side.position[0]);
                auto const  point1 = vector_from_brush_position(side.position[1]);
                auto const  point2 = vector_from_brush_position(side.position[2]);
                auto        winding = winding_from_points(point0, point1, point2, in_baseSize);

                // chop the winding against all other sides
                for (int j = 0; j < in_brush.sides.size(); ++j) {
                    if (i == j) {
                        continue;
                    }
                    winding = chop_winding(winding, planes[j], 0.01);
                }

                // add the final winding
                result.push_back(winding);
            }

            return(result);
        }


        //
    // create_brush_side
    //

        static BrushSide create_brush_side(BrushPosition const &in_point0, BrushPosition const &in_point1, BrushPosition const &in_point2) {//, std::string const &in_material) {
            BrushSide  result;
            result.position[0] = in_point0;
            result.position[1] = in_point1;
            result.position[2] = in_point2;/*
            result.material = in_material;
            result.textureAngle = 0.0;
            result.textureScale[0] = 0.25;
            result.textureScale[1] = 0.25;
            result.textureTranslation[0] = 0.0;
            result.textureTranslation[1] = 0.0;*/
            return(result);
        }


        //
        // create_box_brush
        //

        Brush create_box_brush(BrushPosition const &in_min, BrushPosition const &in_max) {//, std::string const &in_material) {
            auto const  left = create_brush_side({ in_min.x, in_max.y, in_max.z }, { in_min.x, in_max.y, in_min.z }, { in_min.x, in_min.y, in_min.z });//, in_material);
            auto const  right = create_brush_side({ in_max.x, in_min.y, in_max.z }, { in_max.x, in_min.y, in_min.z }, { in_max.x, in_max.y, in_min.z });//, in_material);

            auto const  front = create_brush_side({ in_min.x, in_min.y, in_max.z }, { in_min.x, in_min.y, in_min.z }, { in_max.x, in_min.y, in_min.z });//, in_material);
            auto const  back = create_brush_side({ in_max.x, in_max.y, in_max.z }, { in_max.x, in_max.y, in_min.z }, { in_min.x, in_max.y, in_min.z });//, in_material);

            auto const  bottom = create_brush_side({ in_min.x, in_min.y, in_min.z }, { in_min.x, in_max.y, in_min.z }, { in_max.x, in_max.y, in_min.z });//, in_material);
            auto const  top = create_brush_side({ in_min.x, in_max.y, in_max.z }, { in_min.x, in_min.y, in_max.z }, { in_max.x, in_min.y, in_max.z });//, in_material);

            Brush  result;
            result.type = BrushType::Additive;
            result.content = BrushContent::Solid;
            result.textureMode = BrushTextureMode::Global;
            result.sides = { left, right, front, back, bottom, top };
            return(result);
        }


        //
        // is_point_inside_brush
        //

        bool is_point_inside_brush(Brush const &in_brush, math::Vector3 const &in_point, double const in_epsilon) {
            for (auto const &side : in_brush.sides) {
                auto const  plane = plane_from_brush_side(side);  // TODO: this is not efficient
                if (plane.distance_from(in_point) > in_epsilon) {
                    return(false);
                }
            }
            return(true);
        }


        //
        // intersect_ray_with_brush
        //
        // returns -1 if the brush was not hit or the distance along the ray otherwise
        //
        
        double intersect_ray_with_brush(Brush const &in_brush, math::Ray const &in_ray, double const in_epsilon) {
            auto  enter = -std::numeric_limits<double>::infinity();

            for (auto const &side : in_brush.sides) {
                auto const  plane = plane_from_brush_side(side);  // TODO: this is not efficient
                auto const  dot = math::dot(plane.normal(), in_ray.direction());
                if (dot < -0.00001) {
                    auto const  dist = plane.distance_from(in_ray.origin());
                    if (dist >= 0.0) {
                        enter = std::max(enter, dist / -dot);
                    }
                }
            }

            if (enter >= 0.0) {
                auto const  point = in_ray.calculate_point(enter);
                if (is_point_inside_brush(in_brush, point, in_epsilon)) {
                    return(enter);
                }
            }

            return(-1.0);
        }
    }
}