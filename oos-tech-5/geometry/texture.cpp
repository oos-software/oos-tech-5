/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <tuple>

#include <math/math.hpp>
#include <geometry/texture.hpp>


namespace oos {
    namespace geometry {
        static std::tuple<math::Vector3, math::Vector3, math::Vector3> const  s_textureAxes[] = {
            { math::Vector3(-1, 0, 0), math::Vector3(0, -1, 0), math::Vector3(0, 0, -1) },
            { math::Vector3(1, 0, 0), math::Vector3(0, 1, 0), math::Vector3(0, 0, -1) },
            { math::Vector3(0, -1, 0), math::Vector3(1, 0, 0), math::Vector3(0, 0, -1) },
            { math::Vector3(0, 1, 0), math::Vector3(-1, 0, 0), math::Vector3(0, 0, -1) },
            { math::Vector3(0, 0, -1), math::Vector3(1, 0, 0), math::Vector3(0, 1, 0) },
            { math::Vector3(0, 0, 1), math::Vector3(1, 0, 0), math::Vector3(0, -1, 0) }
        };


        //
        // texture_axes_for_normal
        //

        std::pair<math::Vector3, math::Vector3> texture_axes_for_normal(math::Vector3 const &in_normal) {
            int   index = 0;
            auto  largest = math::dot(std::get<0>(s_textureAxes[0]), in_normal);

            for (int i = 1; i < 6; ++i) {
                auto const  d = math::dot(std::get<0>(s_textureAxes[i]), in_normal);
                if (d > largest) {
                    index = i;
                    largest = d;
                }
            }

            return(std::make_pair(std::get<1>(s_textureAxes[index]), std::get<2>(s_textureAxes[index])));
        }


        //
        // texture_axes_for_normal
        //

        std::pair<math::Vector3, math::Vector3> texture_axes_for_normal(math::Vector3 const &in_normal, double const in_angle) {
            int   index = 0;
            auto  largest = math::dot(std::get<0>(s_textureAxes[0]), in_normal);

            for (int i = 1; i < 6; ++i) {
                auto const  d = math::dot(std::get<0>(s_textureAxes[i]), in_normal);
                if (d > largest) {
                    index = i;
                    largest = d;
                }
            }

            int  first, second;
            switch (index >> 1) {
            case 0:
                first = 1;
                second = 2;
                break;

            case 1:
                first = 0;
                second = 2;
                break;

            case 2:
                first = 0;
                second = 1;
                break;
            }

            auto  uAxis = std::get<1>(s_textureAxes[index]);
            auto  vAxis = std::get<2>(s_textureAxes[index]);

            if (0.0 != in_angle) {
                auto const  a = math::to_radians(in_angle);
                auto const  s = sin(a);
                auto const  c = cos(a);
                auto const  ux = uAxis.get(first) * c - uAxis.get(second) * s;
                auto const  uy = uAxis.get(first) * s + uAxis.get(second) * c;
                uAxis = uAxis.replace(first, ux).replace(second, uy);
                auto const  vx = vAxis.get(first) * c - vAxis.get(second) * s;
                auto const  vy = vAxis.get(first) * s + vAxis.get(second) * c;
                vAxis = vAxis.replace(first, vx).replace(second, vy);
            }

            return(std::make_pair(uAxis, vAxis));
        }

        /*
        //
        // calculate_texture_uv
        //

        math::Vector2 calculate_texture_uv(std::pair<math::Vector3, math::Vector3> const &in_axes, math::Vector3 const &in_point) {
            auto const  u = math::dot(in_point, in_axes.first);
            auto const  v = math::dot(in_point, in_axes.second);
            return(math::Vector2(u, v));
        }


        //
        // calculate_texture_st
        //

        math::Vector2 calculate_texture_st(math::Vector2 const &in_uv, math::Vector2 const &in_scale, math::Vector2 const &in_translation) {
            return(in_uv * in_scale + in_translation);
        }*/
    }
}