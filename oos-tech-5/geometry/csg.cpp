/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <limits.hpp>
#include <geometry/brush.hpp>
#include <geometry/csg.hpp>


namespace oos {
    namespace geometry {
        static double constexpr  Epsilon = 0.01;
        static double constexpr  BaseSize = MaxWorldCoordinate - MinWorldCoordinate;


        // ======================================================================================
        //
        //  CSGSide
        //
        // ======================================================================================
        enum class CSGSide {
            Front,
            Back,
            Both,
            On
        };


        //
        // clone_brush_side_inverted
        //

        static BrushSide clone_brush_side_inverted(BrushSide const &in_side) {
            BrushSide  result = in_side;
            result.position[0] = in_side.position[2];
            result.position[1] = in_side.position[1];
            result.position[2] = in_side.position[0];
            return(result);
        }

        
        //
        // classify_winding
        //
        // returns the side of the winding
        //

        static CSGSide classify_winding(BrushSide const &in_side, Winding const &in_winding) {
            int         numFront = 0;
            int         numBack = 0;

            auto const  plane = plane_from_brush_side(in_side);

            for (auto const &point : in_winding) {
                auto const  dist = plane.distance_from(point);
                if (dist > Epsilon) {
                    ++numFront;
                }
                else if (dist < -Epsilon) {
                    ++numBack;
                }
            }

            if (numFront > 0 && 0 == numBack) {
                return(CSGSide::Front);
            }

            if (numBack > 0 && 0 == numFront) {
                return(CSGSide::Back);
            }

            if (numFront > 0 && numBack > 0) {
                return(CSGSide::Both);
            }

            return(CSGSide::On);
        }

        
        //
        // test_brush_overlap
        //

        static bool test_brush_overlap(Brush const &in_brush, std::list<Winding> const &in_windings) {
            for (auto const &side : in_brush.sides) {
                int         numFront = 0;
                int         numTotal = 0;

                auto const  plane = plane_from_brush_side(side);

                for (auto const &winding : in_windings) {
                    for (auto const &point : winding) {
                        auto const  dist = plane.distance_from(point);
                        if (dist > -Epsilon) {
                            ++numFront;
                        }
                    }
                    numTotal += int(winding.size());
                }

                if (numFront == numTotal) {
                    return(false);
                }
            }

            return(true);
        }


        //
        // are_brushes_overlapping
        //

        static bool are_brushes_overlapping(Brush const &in_cutter,
            std::list<Winding> const &in_cutterWindings, Brush const &in_brush, std::list<Winding> const &in_windings) {
            return(test_brush_overlap(in_cutter, in_windings) && test_brush_overlap(in_brush, in_cutterWindings));
        }


        //
        // find_relevant_brush_sides
        //

        static std::list<BrushSide> find_relevant_brush_sides(Brush const &in_cutter,
            std::list<Winding> const &in_cutterWindings, Brush const &in_brush, std::list<Winding> const &in_windings) {
            size_t                index = 0;
            std::list<BrushSide>  result;

            for (auto const &cutterWinding : in_cutterWindings) {
                bool        valid = true;
                auto const  &side = in_cutter.sides[index++];

                // if the cutter winding is in front of a single side of the brush, the cutter side is not valid
                for (auto const &otherSide : in_brush.sides) {
                    if (CSGSide::Front == classify_winding(otherSide, cutterWinding)) {
                        valid = false;
                        break;
                    }
                }
                if (!valid) {
                    continue;
                }

                // the cutter side needs to split a winding of the brush to be valid
                valid = false;
                for (auto const &winding : in_windings) {
                    if (CSGSide::Both == classify_winding(side, winding)) {
                        valid = true;
                        break;
                    }
                }
                if (!valid) {
                    continue;
                }

                result.push_back(side);
            }

            return(result);
        }

        
        //
        // split_brush
        //
        // returns the brush behind the plane or nullptr if the brush was completely cut away
        //

        static Brush split_brush(std::list<Brush> &in_result, BrushSide const &in_side, Brush const &in_brush) {
            std::list<BrushSide>  frontSides;
            std::list<BrushSide>  backSides;

            size_t                index = 0;
            auto const            normal = plane_from_brush_side(in_side).normal();
            auto const            windings = create_brush_windings(in_brush, BaseSize);

            for (auto const &winding : windings) {
                auto const  &side = in_brush.sides[index++];
                auto const  sideNormal = plane_from_brush_side(side).normal();

                switch (classify_winding(in_side, winding)) {
                case CSGSide::Front:
                    frontSides.push_back(side);
                    break;

                case CSGSide::Back:
                    backSides.push_back(side);
                    break;

                case CSGSide::Both:
                    frontSides.push_back(side);
                    backSides.push_back(side);
                    break;

                case CSGSide::On:
                    if (math::dot(normal, sideNormal) > 0) {
                        frontSides.push_back(side);
                    }
                    else {
                        backSides.push_back(side);
                    }
                    break;
                }
            }

            // the cutting plane is contained in both the new brushes (front needs to be inverted)
            frontSides.push_back(clone_brush_side_inverted(in_side));

            backSides.push_back(in_side);
            //backSides.back().material = s_noDrawMaterial;  // no lightmap and drawing needed for the back side

            // create the front brush (if valid)
            if (frontSides.size() >= 4) {
                Brush  front;
                front.type = BrushType::Additive;
                front.content = in_brush.content;
                front.sides = std::vector<BrushSide>(frontSides.begin(), frontSides.end());
                in_result.push_back(front);
            }

            // create the back brush (if valid)
            if (backSides.size() >= 4) {
                Brush  back;
                back.type = BrushType::Additive;
                back.content = in_brush.content;
                back.sides = std::vector<BrushSide>(backSides.begin(), backSides.end());
                return(back);  // back brush needs to be cutted further
            }

            return {};
        }
        

        //
        // perform_csg_subtraction
        //

        static std::list<Brush> perform_csg_subtraction(Brush const &in_cutter, std::list<Brush> const &in_brushes) {
            std::list<Brush>  result;
            auto const        cutterWindings = create_brush_windings(in_cutter, BaseSize);

            for (auto const &brush : in_brushes) {
                auto const  windings = create_brush_windings(brush, BaseSize);

                // check for overlap
                if (!are_brushes_overlapping(in_cutter, cutterWindings, brush, windings)) {
                    result.push_back(brush);
                    continue;  // no cutting needs to be done
                }

                // find the relevant brush sides
                auto const  sides = find_relevant_brush_sides(in_cutter, cutterWindings, brush, windings);
                if (sides.empty()) {
                    printf("WARNING: no relevant brush sides found!\n");
                    result.push_back(brush);
                    continue;
                }
                //printf("%u relevant brush sides...\n", uint32_t(sides.size()));

                // split the brush (the brush on the front is finished, the brush on the back will be cutted further)
                auto  toCut = brush;
                for (auto const &side : sides) {
                    toCut = split_brush(result, side, toCut);
                    if (toCut.sides.empty()) {
                        break;
                    }
                }
            }

            return(result);
        }


        //
        // rebuild_brush_geometry
        //

        std::list<Brush> rebuild_brush_geometry(std::list<Brush> const &in_brushes) {
            std::list<Brush>  toCut;
            std::list<Brush>  brushes = in_brushes;
            auto const        baseSize = 

            printf("before CSG = %llu brushes...\n", brushes.size());

            while (true) {
                // find the first subtractive brush (if any)
                Brush  cutter;
                bool   cutterFound = false;

                for (auto it = brushes.begin(); it != brushes.end() && !cutterFound; ) {
                    if (BrushType::Subtractive == it->type) {
                        cutter = *it;
                        cutterFound = true;
                    }
                    else if (BrushType::Additive == it->type) {
                        toCut.push_back(*it);
                    }
                    brushes.erase(it++);
                }
                if (!cutterFound) {
                    break;  // no subtractive brush found
                }

                // perform CSG subtraction for all brushes which came before the cutter
                toCut = perform_csg_subtraction(cutter, toCut);
            }

            printf("after CSG = %llu brushes...\n", toCut.size());

            //auto const  end = get_time();
            //printf("rebuild_brush_geometry() -> %.3f seconds...\n", end - start);

            return(toCut);
        }
    }
}