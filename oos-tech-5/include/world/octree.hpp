/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include <functional>

#include "limits.hpp"

/*
an object is inserted into the first node that splits it or inside a leaf if it fits inside
*/

namespace oos {
    // ======================================================================================
    //
    //  OctreeObject
    //
    // ======================================================================================
    template <class T> struct OctreeObject {
        ~OctreeObject() {
            delete next;
        }

        T                *data;  // not owned
        OctreeObject<T>  *next;  // singly linked list
    };


    // ======================================================================================
    //
    //  OctreeNode
    //
    // ======================================================================================
    template <class T> struct OctreeNode {
        ~OctreeNode() {
            for (int i = 0; i < 8; ++i) {
                delete children[i];
            }
            delete objects;
        }

        OctreeNode<T>    *children[8] = {};   // not all need to be set
        OctreeObject<T>  *objects = nullptr;
    };


    // ======================================================================================
    //
    //  Octree
    //
    // ======================================================================================
    template <class T> class Octree {
        static int constexpr     MinOctreeSize = 512;  // if a node is this size, we stop subdivision
        static double constexpr  BoundingEpsilon = 0.125;


    public:
        void insert(double const in_minX, double const in_minY, double const in_minZ, double const in_maxX, double const in_maxY, double const in_maxZ, T *in_data);
        void remove(double const in_minX, double const in_minY, double const in_minZ, double const in_maxX, double const in_maxY, double const in_maxZ, T *in_data);

        void visit(double const in_minX, double const in_minY, double const in_minZ, double const in_maxX, double const in_maxY, double const in_maxZ, std::function<void(T *)> const &in_callback);


    private:
        

        void remove_recursive(int in_treeMinX, int in_treeMinY, int in_treeMinZ, int in_treeMaxX, int in_treeMaxY, int in_treeMaxZ,
            int const in_minX, int const in_minY, int const in_minZ, int const in_maxX, int const in_maxY, int const in_maxZ, OctreeNode<T> *in_currentNode, T *in_data);


    private:
        OctreeNode<T>  m_rootNode;


    private:
        static void adjust_bounding_min(int &in_minX, int &in_minY, int &in_minZ, double const in_boundingMinX, double const in_boundingMinY, double const in_boundingMinZ);
        static void adjust_bounding_max(int &in_maxX, int &in_maxY, int &in_maxZ, double const in_boundingMaxX, double const in_boundingMaxY, double const in_boundingMaxZ);
        
        static int determine_child_number(int const in_centerX, int const in_centerY, int const in_centerZ, int const in_posX, int const in_posY, int const in_posZ);

        static void insert_object(OctreeNode<T> *in_node, T *in_data);
        static void remove_object(OctreeNode<T> *in_node, T *in_data);
    };


    //
    // adjust_bounding_min
    //

    template <class T> void Octree<T>::adjust_bounding_min(int &in_minX, int &in_minY, int &in_minZ, double const in_boundingMinX, double const in_boundingMinY, double const in_boundingMinZ) {
        in_minX = int(std::floor(in_boundingMinX - BoundingEpsilon));
        in_minY = int(std::floor(in_boundingMinY - BoundingEpsilon));
        in_minZ = int(std::floor(in_boundingMinZ - BoundingEpsilon));
    }


    //
    // adjust_bounding_max
    //

    template <class T> void Octree<T>::adjust_bounding_max(int &in_maxX, int &in_maxY, int &in_maxZ, double const in_boundingMaxX, double const in_boundingMaxY, double const in_boundingMaxZ) {
        in_maxX = int(std::ceil(in_boundingMaxX + BoundingEpsilon));
        in_maxY = int(std::ceil(in_boundingMaxY + BoundingEpsilon));
        in_maxZ = int(std::ceil(in_boundingMaxZ + BoundingEpsilon));
    }


    //
    // determine_child_number
    //

    template <class T> int Octree<T>::determine_child_number(int const in_centerX, int const in_centerY, int const in_centerZ, int const in_posX, int const in_posY, int const in_posZ) {
        int  result = 0;
        if (in_posX >= in_centerX) {
            result += 1;
        }
        if (in_posY >= in_centerY) {
            result += 2;
        }
        if (in_posZ >= in_centerZ) {
            result += 4;
        }
        return(result);
    }


    //
    // insert_object
    //

    template <class T> void Octree<T>::insert_object(OctreeNode<T> *in_node, T *in_data) {
        auto  object = new OctreeObject<T>();
        object->data = in_data;
        object->next = in_node->objects;
        in_node->objects = object;
    }


    //
    // remove_object
    //

    template <class T> void Octree<T>::remove_object(OctreeNode<T> *in_node, T *in_data) {
        OctreeObject<T>  *prev = nullptr;
        auto             object = in_node->objects;

        while (nullptr != object) {
            if (object->data == in_data) {
                if (nullptr == prev) {
                    in_node->objects = object->next;
                }
                else {
                    prev->next = object->next;
                }
                break;
            }
            prev = object;
            object = object->next;
        }
    }


    //
    // insert
    //

    template <class T> void Octree<T>::insert(double const in_minX, double const in_minY, double const in_minZ, double const in_maxX, double const in_maxY, double const in_maxZ, T *in_data) {
        int  minX, minY, minZ;
        adjust_bounding_min(minX, minY, minZ, in_minX, in_minY, in_minZ);

        int  maxX, maxY, maxZ;
        adjust_bounding_max(maxX, maxY, maxZ, in_maxX, in_maxY, in_maxZ);

        int  treeMinX = MinWorldCoordinate;
        int  treeMinY = MinWorldCoordinate;
        int  treeMinZ = MinWorldCoordinate;

        int  treeMaxX = MaxWorldCoordinate;
        int  treeMaxY = MaxWorldCoordinate;
        int  treeMaxZ = MaxWorldCoordinate;

        auto  *current = &m_rootNode;

        while (true) {
            int const  size = treeMaxX - treeMinX;

            if (size <= MinOctreeSize) {
                insert_object(current, in_data);
                break;
            }

            int const  centerX = (treeMinX + treeMaxX) >> 1;
            int const  centerY = (treeMinY + treeMaxY) >> 1;
            int const  centerZ = (treeMinZ + treeMaxZ) >> 1;

            int const  minChild = determine_child_number(centerX, centerY, centerZ, minX, minY, minZ);
            int const  maxChild = determine_child_number(centerX, centerY, centerZ, maxX, maxY, maxZ);

            if (minChild != maxChild) {
                insert_object(current, in_data);
                break;
            }

            if (0 == (minChild & 1)) {
                treeMaxX = centerX;
            }
            else {
                treeMinX = centerX;
            }

            if (minChild < 4) {
                treeMaxZ = centerZ;
            }
            else {
                treeMinZ = centerZ;
            }

            if (0 == ((minChild >> 1) & 1)) {
                treeMaxY = centerY;
            }
            else {
                treeMinY = centerY;
            }

            if (nullptr == current->children[minChild]) {
                current->children[minChild] = new OctreeNode<T>();
            }
            current = current->children[minChild];
        }
    }


    //
    // remove
    //

    template <class T> void Octree<T>::remove(double const in_minX, double const in_minY, double const in_minZ, double const in_maxX, double const in_maxY, double const in_maxZ, T *in_data) {
        int  minX, minY, minZ;
        adjust_bounding_min(minX, minY, minZ, in_minX, in_minY, in_minZ);

        int  maxX, maxY, maxZ;
        adjust_bounding_max(maxX, maxY, maxZ, in_maxX, in_maxY, in_maxZ);

        remove_recursive(MinWorldCoordinate, MinWorldCoordinate, MinWorldCoordinate, MaxWorldCoordinate, MaxWorldCoordinate, MaxWorldCoordinate, minX, minY, minZ, maxX, maxY, maxZ, &m_rootNode, in_data);
        /*
        int  treeMinX = MinWorldCoordinate;
        int  treeMinY = MinWorldCoordinate;
        int  treeMinZ = MinWorldCoordinate;

        int  treeMaxX = MaxWorldCoordinate;
        int  treeMaxY = MaxWorldCoordinate;
        int  treeMaxZ = MaxWorldCoordinate;

        auto  *current = &m_rootNode;

        while (true) {
            int const  centerX = (treeMinX + treeMaxX) >> 1;
            int const  centerY = (treeMinY + treeMaxY) >> 1;
            int const  centerZ = (treeMinZ + treeMaxZ) >> 1;

            int const  minChild = determine_child_number(centerX, centerY, centerZ, minX, minY, minZ);
            int const  maxChild = determine_child_number(centerX, centerY, centerZ, maxX, maxY, maxZ);

            if (minChild != maxChild) {
                remove_object(current, in_data);
                break;
            }

            if (0 == (minChild & 1)) {
                treeMaxX = centerX;
            }
            else {
                treeMinX = centerX;
            }

            if (minChild < 4) {
                treeMaxZ = centerZ;
            }
            else {
                treeMinZ = centerZ;
            }

            if (0 == ((minChild >> 1) & 1)) {
                treeMaxY = centerY;
            }
            else {
                treeMinY = centerY;
            }

            auto  child = current->children[minChild];
            if (nullptr == child) {
                remove_object(current, in_data);

                break;
            }
            current = child;
        }*/
    }


    //
    // remove_recursive
    //

    template <class T> void Octree<T>::remove_recursive(int in_treeMinX, int in_treeMinY, int in_treeMinZ, int in_treeMaxX, int in_treeMaxY, int in_treeMaxZ,
        int const in_minX, int const in_minY, int const in_minZ, int const in_maxX, int const in_maxY, int const in_maxZ, OctreeNode<T> *in_currentNode, T *in_data) {
        int const  centerX = (in_treeMinX + in_treeMaxX) >> 1;
        int const  centerY = (in_treeMinY + in_treeMaxY) >> 1;
        int const  centerZ = (in_treeMinZ + in_treeMaxZ) >> 1;

        int const  minChild = determine_child_number(centerX, centerY, centerZ, in_minX, in_minY, in_minZ);
        int const  maxChild = determine_child_number(centerX, centerY, centerZ, in_maxX, in_maxY, in_maxZ);

        if (minChild != maxChild) {
            remove_object(in_currentNode, in_data);
            return;
        }

        auto  child = in_currentNode[minChild];
        if (nullptr == child) {
            remove_object(in_currentNode, in_data);
            return;
        }

        if (0 == (minChild & 1)) {
            in_treeMaxX = centerX;
        }
        else {
            in_treeMinX = centerX;
        }

        if (minChild < 4) {
            in_treeMaxZ = centerZ;
        }
        else {
            in_treeMinZ = centerZ;
        }

        if (0 == ((minChild >> 1) & 1)) {
            in_treeMaxY = centerY;
        }
        else {
            in_treeMinY = centerY;
        }

        remove_recursive(in_treeMinX, in_treeMinY, in_treeMinZ, in_treeMaxX, in_treeMaxY, in_treeMaxZ, in_minX, in_minY, in_minZ, in_maxX, in_maxY, in_maxZ, child, in_data);
        
        for (int i = 0; i < 8; ++i) {
            if (nullptr != child->children[i]) {
                return;
            }
        }
        delete in_currentNode->children[minChild];
        in_currentNode->children[minChild] = nullptr;
    }


    //
    // visit
    //

    template <class T> void Octree<T>::visit(double const in_minX, double const in_minY, double const in_minZ, double const in_maxX, double const in_maxY, double const in_maxZ, std::function<void(T *)> const &in_callback) {

    }
}