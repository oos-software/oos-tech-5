/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include <array>

#include "octree.hpp"
#include "entity.hpp"
#include "../geometry/brush.hpp"
#include "../light.hpp"
#include "renderer/face.hpp"
#include "lightmapper/lightmapper.hpp"

/*
a world is (more ore less) just a container for the various data
*/

namespace oos {
    // ======================================================================================
    //
    //  WorldOctreeData
    //
    // ======================================================================================
    struct WorldOctreeData {/*
        enum class Type {
            Brush,
            Entity,
            Terrain
        };
        Type  type;
        union {
            Brush    *brush;
            Entity   *entity;
            Terrain  *terrain;
        };*/
    };
    

    // ======================================================================================
    //
    //  WorldLightMap
    //
    // ======================================================================================
    struct WorldLightMap {
        uint16_t            width;
        uint16_t            height;
        std::vector<float>  luxels;  // TODO: change to fixed point/half float
    };

    
    // ======================================================================================
    //
    //  World
    //
    // ======================================================================================
    struct World2 {
        // entities
        virtual EntityId spawn_entity() = 0;
        virtual void kill_entity(EntityId const in_entity) = 0;
        virtual Entity *get_entity(EntityId const in_entity) = 0;
    };


    // ======================================================================================
    //
    //  World
    //
    // ======================================================================================
    class World {
    public:
        World();
        ~World();

        // brushes
        std::list<geometry::Brush> const &get_brushes() const {
            return(m_brushes);
        }

        void add_brush(geometry::Brush const &in_brush);
        //void remove_brush();
        //void update_brush();
        /*
        // directional lights
        std::list<DirectionalLight> const &get_directional_lights() const {
            return(m_directionalLights);
        }

        void add_directional_light(DirectionalLight const &in_light);
        
        // point lights
        std::list<PointLight> const &get_points_lights() const {
            return(m_pointLights);
        }

        void add_point_light(PointLight const &in_light);

        // spot lights
        std::list<SpotLight> const &get_spot_lights() const {
            return(m_spotLights);
        }

        void add_spot_light(SpotLight const &in_light);
        */
        // built brushes
        std::vector<geometry::Brush> const &get_built_brushes() const {
            return(m_builtBrushes);
        }

        void set_built_brushes(std::list<geometry::Brush> const &in_brushes) {
            m_builtBrushes = std::vector<geometry::Brush>(in_brushes.begin(), in_brushes.end());
        }

        // brush vertexes
        std::vector<math::Vector3> const &get_brush_vertexes() {
            return(m_brushVertexes);
        }

        void set_brush_vertexes(std::vector<math::Vector3> const &in_vertexes) {
            m_brushVertexes = in_vertexes;
        }

        // brush faces
        std::vector<Face> &get_brush_faces() {
            return(m_brushFaces);
        }

        std::vector<Face> const &get_brush_faces() const {
            return(m_brushFaces);
        }

        void set_brush_faces(std::list<Face> const &in_faces) {
            m_brushFaces = std::vector<Face>(in_faces.begin(), in_faces.end());
        }
        /*
        // light maps
        std::vector<WorldLightMap> const &get_light_maps() const {
            return(m_lightMaps);
        }

        void set_light_maps(std::vector<WorldLightMap> const &in_lightMaps) {
            m_lightMaps = in_lightMaps;
        }
        */

    private:
        Octree<WorldOctreeData>          m_octree;
        std::list<geometry::Brush>       m_brushes;

        size_t                           m_numberOfEntities;
        std::array<Entity, MaxEntities>  m_entities;

        // all lights inside the world are static and used for lightmapping (dynamic lights are handled elsewhere)
        //std::list<DirectionalLight>      m_directionalLights;
        //std::list<PointLight>            m_pointLights;
        //std::list<SpotLight>             m_spotLights;

        // the following values are only valid after 'rebuild_world_geometry'
        std::vector<geometry::Brush>     m_builtBrushes;
        std::vector<math::Vector3>       m_brushVertexes;
        std::vector<Face>                m_brushFaces;

        // only valid after 'rebuild_world_lighting'
        //std::vector<WorldLightMap>       m_lightMaps;
    };


    //
    // function declarations
    //

    void rebuild_world(World *in_world, bool const in_calculateLighting, LightmapperQuality const in_quality);
}