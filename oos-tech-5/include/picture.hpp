/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include <vector>


namespace oos {
    // ======================================================================================
    //
    //  Picture
    //
    // ======================================================================================
    class Picture {
    public:
        Picture(int const in_width, int const in_height, uint8_t const *in_pixels);

        int const width() const {
            return(m_width);
        }

        int const height() const {
            return(m_height);
        }

        std::vector<uint8_t> const &pixels() const {
            return(m_pixels);
        }

        bool write_png(std::string const &in_fileName) const;
        bool write_jpeg(std::string const &in_fileName, int const in_quality = 90) const;


    private:
        int const             m_width;
        int const             m_height;
        std::vector<uint8_t>  m_pixels;


    public:
        static Picture *from_file(std::string const &in_fileName);
        static Picture *from_memory(std::vector<uint8_t> const &in_data);
    };
}