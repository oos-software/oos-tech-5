/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


namespace oos {
    // ======================================================================================
    //
    //  InputSystem
    //
    // ======================================================================================
    class InputSystem {
    public:
        //void register_binding(std::string const &in_name, InputKey const in_firstKey, InputKey const in_secondKey);
        //void write_bindings();

        bool is_key_press(InputKey const in_key) const;
        bool is_key_release(InputKey const in_key) const;
        bool is_key_down(InputKey const in_key) const;

        void update(InputDevice *in_inputDevice);


    private:
        uint8_t  m_keyFlags[int(InputKey::NumInputKeys)] = {};
    };
}