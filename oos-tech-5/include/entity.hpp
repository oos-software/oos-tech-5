/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include "math/vector.hpp"
#include "math/quaternion.hpp"


namespace oos {
    using EntityId = uint32_t;


    int constexpr       MaxEntities = 100000;
    EntityId constexpr  InvalidEntityId = EntityId(~0);
    

    // ======================================================================================
    //
    //  Entity
    //
    // ======================================================================================
    struct Entity {
        EntityId          id = InvalidEntityId;
        math::Vector3     origin;
        math::Quaternion  rotation;
        math::Vector3     scale;
    };
}