/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


namespace oos {
    // ======================================================================================
    //
    //  AssetType
    //
    // ======================================================================================
    enum class AssetType {
        Level,
        Material,
        Sample,
        Stream,
        
        NumAssetTypes
    };
}