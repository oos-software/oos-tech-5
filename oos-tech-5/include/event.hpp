/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include <vector>


namespace oos {
    // ======================================================================================
    //
    //  Event
    //
    // ======================================================================================
    struct Event {
        uint32_t  id;
    };


    // ======================================================================================
    //
    //  EventListener
    //
    // ======================================================================================
    struct EventListener {
        virtual void handle_event(Event const &in_event) = 0;
    };


    // ======================================================================================
    //
    //  EventDispatcher
    //
    // ======================================================================================
    class EventDispatcher {
    public:
        void register_listener(uint32_t const in_id, EventListener *in_listener);

        void dispatch_event(Event const &in_event);


    private:
        std::vector<std::vector<EventListener *>>  m_listeners;
    };
}