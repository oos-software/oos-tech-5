/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include "ray.hpp"


namespace oos {
    namespace math {
        // ======================================================================================
        //
        //  Plane
        //
        // ======================================================================================
        struct Plane {
            explicit Plane();
            explicit Plane(Vector3 const &in_normal, double const in_distance);
            explicit Plane(Vector3 const &in_normal, Vector3 const &in_point);

            Vector3 const &normal() const {
                return(m_normal);
            }

            double distance() const {
                return(m_distance);
            }

            double distance_from(Vector3 const &in_point) const;

            double intersect_ray(Ray const &in_ray) const;

            Plane negated() const;


        private:
            Vector3  m_normal;
            double   m_distance;  // distance along the normal


        public:
            static Plane from_points(Vector3 const &in_point0, Vector3 const &in_point1, Vector3 const &in_point2);
        };
    }
}