/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include "math.hpp"
#include "vector.hpp"
#include "matrix.hpp"


namespace oos {
    namespace math {
        // ======================================================================================
        //
        //  Quaternion
        //
        // ======================================================================================
        class Quaternion {
        public:
            explicit Quaternion();
            explicit Quaternion(double const in_x, double const in_y, double const in_z, double const in_w);
            explicit Quaternion(Vector3 const &in_point);  // point
            explicit Quaternion(Vector3 const &in_axis, double const in_angle);  // axis angle

            double x() const {
                return(m_x);
            }

            double y() const {
                return(m_y);
            }

            double z() const {
                return(m_z);
            }

            double w() const {
                return(m_w);
            }

            double get(int const in_num) const {
                auto const  *ptr = &m_x;
                return(ptr[in_num]);
            }

            Quaternion conjugate() const {
                return(Quaternion(-m_x, -m_y, -m_z, m_w));
            }

            Quaternion operator *(Quaternion const &in_other) const;
            Vector3 operator *(Vector3 const &in_point) const;
            /*
            Quaternion<T> normalize() const {
                T const  mag = sqrt(m_x * m_x + m_y * m_y + m_z * m_z + m_w * m_w);
                if (mag >= 0.0001) {
                    T const  inv = 1 / mag;
                    return(Quaternion<T>(m_x * inv, m_y * inv, m_z * inv, m_w * inv));
                }
                return(Quaternion<T>());
            }

            Quaternion<T> slerp(Quaternion<T> const a_other, T const a_t) const {
                T        c1temp, c2;
                T const  cosAngle = dot(a_other);

                // lerp for close rotations
                if ((1 - abs(cosAngle)) < 0.01) {
                    c1temp = 1 - a_t;
                    c2 = a_t;
                }
                else {
                    T const  angle = acos(abs(cosAngle));
                    T const  sinAngle = sin(angle);
                    c1temp = sin(angle * (1 - a_t)) / sinAngle;
                    c2 = sin(angle * a_t) / sinAngle;
                }

                // use shortest path
                T const  c1 = (cosAngle < 0) ? -c1temp : c1temp;

                return(Quaternion<T>(c1 * m_x + c2 * a_other.m_x, c1 * m_y + c2 * a_other.m_y, c1 * m_z + c2 * a_other.m_z, c1 * m_w + c2 * a_other.m_w));
            }
            */
            Vector3 to_point() const {
                return(Vector3(m_x, m_y, m_z));
            }

            Matrix3 to_matrix() const;


        private:
            double  m_x;
            double  m_y;
            double  m_z;
            double  m_w;

            /*
        public:
            static Vector3<T> rotate(Vector3<T> const &a_axis, T const a_angle, Vector3<T> const &a_point) {
                Quaternion<T> const  r(a_axis, a_angle);
                Quaternion<T> const  p(a_point);
                return((r * p * r.conjugate()).getPoint());
            }

            static Quaternion<T> from_matrix(Matrix3<T> const &a_matrix) {
                T const  x = T(sqrt(std::max(0.0, 1.0 + a_matrix.getValue(0) - a_matrix.getValue(4) - a_matrix.getValue(8))) * 0.5);
                T const  y = T(sqrt(std::max(0.0, 1.0 - a_matrix.getValue(0) + a_matrix.getValue(4) - a_matrix.getValue(8))) * 0.5);
                T const  z = T(sqrt(std::max(0.0, 1.0 - a_matrix.getValue(0) - a_matrix.getValue(4) + a_matrix.getValue(8))) * 0.5);
                T const  w = T(sqrt(std::max(0.0, 1.0 + a_matrix.getValue(0) + a_matrix.getValue(4) + a_matrix.getValue(8))) * 0.5);
                T const  qx = std::copysign(x, a_matrix.getValue(7) - a_matrix.getValue(5));
                T const  qy = std::copysign(y, a_matrix.getValue(2) - a_matrix.getValue(6));
                T const  qz = std::copysign(z, a_matrix.getValue(3) - a_matrix.getValue(1));
                return(Quaternion<T>(qx, qy, qz, -w));
            }
        };


        template <class T> std::ostream &operator <<(std::ostream &a_stream, Quaternion<T> const &a_quat) {
            a_stream << "Quaternion(" << a_quat.x() << ", " << a_quat.y() << ", " << a_quat.z() << ", " << a_quat.w() << ")" << std::endl;
            return(a_stream);
        }*/
        };


        //
        // function declarations
        //

        double dot(Quaternion const &in_quat0, Quaternion const &in_quad1);
    }
}