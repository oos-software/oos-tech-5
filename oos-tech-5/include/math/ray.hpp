/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include "vector.hpp"


namespace oos {
    namespace math {
        // ======================================================================================
        //
        //  Ray
        //
        // ======================================================================================
        class Ray {
        public:
            explicit Ray(Vector3 const &in_origin, Vector3 const &in_direction);  // direction must be normalized

            Vector3 const &origin() const {
                return(m_origin);
            }

            Vector3 const &direction() const {
                return(m_direction);
            }

            Vector3 calculate_point(double const in_t) const {
                return(m_origin + m_direction * in_t);
            }

            double project_point(Vector3 const &in_point) const {
                return(math::dot(in_point - m_origin, m_direction));
            }


        private:
            Vector3  m_origin;
            Vector3  m_direction;
        };
    }
}