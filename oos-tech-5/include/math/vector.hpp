/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include <algorithm>


namespace oos {
    namespace math {
        // ======================================================================================
        //
        //  Vector
        //
        // ======================================================================================
        template <int N, class T> struct Vector {
            static_assert(N >= 2, "A vector must have at least 2 components");

            inline T operator [](int const in_i) const {
                return(m_value[in_i]);
            }

            inline T get(int const in_i) const {
                return(m_value[in_i]);
            }

            inline T const *pointer() const {
                return(m_value);
            }

            int largest_index() const {
                int  index = 0;
                T    max = std::abs(m_value[0]);
                for (int i = 1; i < N; ++i) {
                    T const  val = std::abs(m_value[i]);
                    if (val > max) {
                        index = i;
                        max = val;
                    }
                }
                return(index);
            }

            int second_largest_index(int const in_first) const {
                int  index = 0;
                T    max = -1;
                for (int i = 0; i < N; ++i) {
                    if (in_first == i) {
                        continue;
                    }
                    T const  val = std::abs(m_value[i]);
                    if (val > max) {
                        index = i;
                        max = val;
                    }
                }
                return(index);
            }


        protected:
            T  m_value[N];
        };


        // ======================================================================================
        //
        //  Vector2
        //
        // ======================================================================================
        struct Vector2 : public Vector<2, double> {
            explicit Vector2();
            explicit Vector2(double const in_x, double const in_y);

            inline double x() const {
                return(m_value[0]);
            }

            inline double y() const {
                return(m_value[1]);
            }

            Vector2 operator +(Vector2 const &in_other) const;
            Vector2 operator -(Vector2 const &in_other) const;
            Vector2 operator *(double const in_scalar) const;
            Vector2 operator /(double const in_scalar) const;

            Vector2 operator *(Vector2 const &in_other) const;
        };


        // ======================================================================================
        //
        //  Vector3
        //
        // ======================================================================================
        struct Vector3 : public Vector<3, double> {
            explicit Vector3();
            explicit Vector3(double const in_x, double const in_y, double const in_z);
            explicit Vector3(double const *in_values);

            inline double x() const {
                return(m_value[0]);
            }

            inline double y() const {
                return(m_value[1]);
            }

            inline double z() const {
                return(m_value[2]);
            }

            Vector3 operator +(Vector3 const &in_other) const;
            Vector3 operator -(Vector3 const &in_other) const;
            Vector3 operator *(double const in_scalar) const;
            Vector3 operator /(double const in_scalar) const;

            Vector3 operator *(Vector3 const &in_other) const;
            Vector3 operator *(struct Matrix3 const &in_matrix) const;

            double magnitude() const;
            double squared_magnitude() const;

            Vector3 abs() const;
            Vector3 negated() const;
            Vector3 normalized() const;

            Vector3 perpendicular() const;

            Vector3 replace(int const in_index, double const in_value) const;

            Vector3 snap(double const in_grid) const;
        };


        // ======================================================================================
        //
        //  Vector4
        //
        // ======================================================================================
        struct Vector4 : public Vector<4, double> {
            explicit Vector4();
            explicit Vector4(double const in_x, double const in_y, double const in_z, double const in_w);

            inline double x() const {
                return(m_value[0]);
            }

            inline double y() const {
                return(m_value[1]);
            }

            inline double z() const {
                return(m_value[2]);
            }

            inline double w() const {
                return(m_value[3]);
            }

            Vector4 operator +(Vector4 const &in_other) const;
            Vector4 operator -(Vector4 const &in_other) const;
            Vector4 operator *(double const in_scalar) const;
            Vector4 operator /(double const in_scalar) const;

            Vector4 operator *(struct Matrix4 const &in_matrix) const;

            Vector4 replace(int const in_index, double const in_value) const;
        };


        //
        // function declarations
        //

        double dot(Vector2 const &in_vec0, Vector2 const &in_vec1);
        double dot(Vector3 const &in_vec0, Vector3 const &in_vec1);
        double dot(Vector4 const &in_vec0, Vector4 const &in_vec1);

        double cross(Vector2 const &in_vec0, Vector2 const &in_vec1);
        Vector3 cross(Vector3 const &in_vec0, Vector3 const &in_vec1);
    }
}