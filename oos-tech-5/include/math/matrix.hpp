/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include "vector.hpp"


namespace oos {
    namespace math {
        // ======================================================================================
        //
        //  Matrix
        //
        // ======================================================================================
        template <int N, class T> struct Matrix {
            static_assert(N >= 2, "A matrix must have at least 2 components");

            inline T operator [](int const in_i) const {
                return(m_value[in_i]);
            }

            inline T const *pointer() const {
                return(m_value);
            }


        protected:
            void multiply(Matrix<N, T> const &in_a, Matrix<N, T> const &in_b) {
                T        *dest = m_value;
                T const  *src1 = in_b.m_value;

                for (int i = 0; i < N; ++i, src1 += N) {
                    for (int j = 0; j < N; ++j, ++dest) {
                        T const  *src0 = &in_a.m_value[j];
                        for (int k = 0; k < N; ++k, src0 += N) {
                            *dest += (*src0) * src1[k];
                        }
                    }
                }
            }


        protected:
            T  m_value[N * N];
        };


        // ======================================================================================
        //
        //  Matrix2
        //
        // ======================================================================================
        struct Matrix2 : public Matrix<2, double> {
            explicit Matrix2();
            explicit Matrix2(double const in_00, double const in_01, double const in_10, double const in_11);

            Matrix2 operator *(Matrix2 const &in_other) const;

            Matrix2 transposed() const;
        };


        // ======================================================================================
        //
        //  Matrix3
        //
        // ======================================================================================
        struct Matrix3 : public Matrix<3, double> {
            explicit Matrix3();
            explicit Matrix3(
                double const in_00, double const in_01, double const in_02,
                double const in_10, double const in_11, double const in_12,
                double const in_20, double const in_21, double const in_22);

            Matrix3 operator *(Matrix3 const &in_other) const;

            Matrix3 transposed() const;
        };


        // ======================================================================================
        //
        //  Matrix4
        //
        // ======================================================================================
        struct Matrix4 : public Matrix<4, double> {
            explicit Matrix4();
            explicit Matrix4(
                double const in_00, double const in_01, double const in_02, double const in_03,
                double const in_10, double const in_11, double const in_12, double const in_13,
                double const in_20, double const in_21, double const in_22, double const in_23,
                double const in_30, double const in_31, double const in_32, double const in_33);

            Matrix4 operator *(Matrix4 const &in_other) const;

            Matrix4 transposed() const;
        };


        //
        // function declarations
        //

        Matrix3 scale_matrix(double const in_scale);
        Matrix3 scale_matrix(double const in_xScale, double const in_yScale, double const in_zScale);
        Matrix3 rotation_matrix(int const in_axis, double const in_angle);
        Matrix4 translation_matrix(Vector3 const &in_translation);

        Matrix3 to_matrix3(Matrix4 const &in_matrix);
        Matrix4 to_matrix4(Matrix3 const &in_matrix);
    }
}