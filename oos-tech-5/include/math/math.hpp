/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


namespace oos {
    namespace math {
        struct Vector3;

        
        double constexpr  PI = 3.14159265358979323846;


        //
        // function declarations
        //

        double to_radians(double const in_deg);
        double to_degrees(double const in_rad);

        double clamp_angle(double const in_value);

        template<class T> T clamp(T const in_value, T const in_min, T const in_max) {
            if (in_value < in_min) {
                return(in_min);
            }
            if (in_value > in_max) {
                return(in_max);
            }
            return(in_value);
        }

        template <class T1> inline T1 polynomial_5(T1 const &in_alpha, T1 const &in_beta, double const in_t) {  // c'est un homage � massa Shuda ;-)
            auto const  t3 = in_t * in_t * in_t, t4 = t3 * in_t, t5 = t4 * in_t;
            return(6.0 * (in_beta - in_alpha) * t5 + 15.0 * (in_alpha - in_beta) * t4 + 10.0 * (in_beta - in_alpha) * t3 + in_alpha);
        }

        double closest_point_on_line_fraction(Vector3 const &in_start, Vector3 const &in_end, Vector3 const &in_point, bool const in_clamp = false);
        struct Vector3 closest_point_on_line(Vector3 const &in_start, Vector3 const &in_end, Vector3 const &in_point, bool const in_clamp = false);
    }
}