/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include <functional>

#include <window.hpp>
#include <systemdevice.hpp>
#include <inputdevice.hpp>
#include <renderdevice.hpp>
#include <audiodevice.hpp>

#include "input.hpp"
#include "renderer/renderer.hpp"


namespace oos {
    // ======================================================================================
    //
    //  Engine
    //
    // ======================================================================================
    struct Engine {
        Window        *window;

        SystemDevice  *systemDevice;
        InputDevice   *inputDevice;
        RenderDevice  *renderDevice;
        AudioDevice   *audioDevice;

        InputSystem   *inputSystem;
        RenderSystem  *renderSystem;
    };


    //
    // function declarations
    //

    Engine *create_engine();
    void destroy_engine();

    void run_engine(std::function<void(Engine *)> const &in_callback);
}