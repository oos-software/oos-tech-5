/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include <string>
#include <vector>


namespace oos {
    typedef struct Config;


    //
    // function declarations
    //

    std::string config_value(Config const *in_config, std::string const &in_key);

    Config const *config_from_file(std::string const &in_fileName);
    Config const *config_from_memory(std::vector<uint8_t> const &in_data);
}