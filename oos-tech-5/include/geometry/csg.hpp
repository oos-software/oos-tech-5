/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


namespace oos {
    namespace geometry {
        std::list<Brush> rebuild_brush_geometry(std::list<Brush> const &in_brushes);
    }
}