/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include <vector>
#include <unordered_map>

#include "error.hpp"
#include "../math/vector.hpp"


namespace oos {
    namespace geometry {
        // ======================================================================================
        //
        //  VertexHash
        //
        // ======================================================================================
        class VertexHash {
        public:
            VertexHash(size_t const in_toReserve = 10000, double const in_snap = 0.0625) : m_snap(in_snap) {
                m_vertexes.reserve(in_toReserve);
            }

            std::vector<math::Vector3> const &vertexes() const {
                return(m_vertexes);
            }

            math::Vector3 const &vertex(uint32_t const in_index) const {
                return(m_vertexes[in_index]);
            }

            uint32_t find_vertex(math::Vector3 const &in_position, bool const in_isSafe = false) {
                auto const      snapped = in_position.snap(m_snap);
                int64_t const   x = int64_t(snapped.x());
                int64_t const   y = int64_t(snapped.y());
                int64_t const   z = int64_t(snapped.z());
                uint64_t const  hash = uint64_t((x << 40) + (y << 20) + z);

                // search the vertex
                auto const      range = m_indexesByHash.equal_range(hash);
                for (auto it = range.first; it != range.second; ++it) {
                    auto const  &vertex = m_vertexes[it->second];
                    if (vertex.x() == snapped.x() && vertex.y() == snapped.y() && vertex.z() == snapped.z()) {
                        return(it->second);
                    }
                }

                // in safe mode we aren't allowed to add new vertexes
                if (in_isSafe) {
                    error("Vertex index not found!");
                }

                // the vertex was not found, add a new one
                uint32_t const  newIndex = static_cast<uint32_t>(m_vertexes.size());
                m_indexesByHash.insert(std::make_pair(hash, newIndex));
                m_vertexes.push_back(snapped);
                return(newIndex);
            }


        private:
            double const                                 m_snap;
            std::unordered_multimap<uint64_t, uint32_t>  m_indexesByHash;
            std::vector<math::Vector3>                   m_vertexes;
        };
    }
}