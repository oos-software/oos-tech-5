/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include <vector>

#include "../math/plane.hpp"


namespace oos {
    namespace geometry {
        using Winding = std::vector<math::Vector3>;
        

        //
        // function declarations
        //

        Winding chop_winding(Winding const &in_winding, math::Plane const &in_plane, double const in_epsilon);
        
        Winding winding_from_points(math::Vector3 const &in_point0, math::Vector3 const &in_point1, math::Vector3 const &in_point2, double const in_baseSize);

        math::Plane plane_from_winding(Winding const &in_winding);
    }
}