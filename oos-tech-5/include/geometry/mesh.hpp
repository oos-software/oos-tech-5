/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include <vector>
#include <algorithm>

#include "../math/vector.hpp"


namespace oos {
    namespace geometry {
        // ======================================================================================
        //
        //  MeshVertex
        //
        // ======================================================================================
        struct MeshVertex {
            math::Vector3  position;
            math::Vector3  normal;
        };


        // ======================================================================================
        //
        //  MeshBuilder
        //
        // ======================================================================================
        class Mesh {
        public:


        private:
            std::vector<MeshVertex>  m_vertexes;
            std::vector<MeshFace>    m_faces;
        };
    }
}