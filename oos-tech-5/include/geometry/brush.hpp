/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include <list>

#include "../math/quaternion.hpp"
#include "winding.hpp"


namespace oos {
    namespace geometry {
        // ======================================================================================
        //
        //  BrushType
        //
        // ======================================================================================
        enum class BrushType {
            Additive,
            Subtractive  // subtractive brushes will cut additive brushes during the CSG phase
        };


        // ======================================================================================
        //
        //  BrushContent
        //
        // ======================================================================================
        enum class BrushContent {
            None = 0x00,   // for triggers, rain volumes etc.
            Solid = 0x01,  // solid geometry (will be used for BSP)
            Water = 0x02,  // water (player can swim and drown)
            Slime = 0x04,  // slime (damages the player only a little bit)
            Lava = 0x08    // lava (hurts the player very much)
        };


        // ======================================================================================
        //
        //  BrushTextureMode
        //
        // ======================================================================================
        enum class BrushTextureMode {
            Global,
            Local
        };


        // ======================================================================================
        //
        //  BrushPosition
        //
        // ======================================================================================
        struct BrushPosition {
            int64_t  x, y, z;
        };


        // ======================================================================================
        //
        //  BrushSide
        //
        // ======================================================================================
        struct BrushSide {
            BrushPosition  position[3];  // the three points defining the plane
        };


        // ======================================================================================
        //
        //  Brush
        //
        // ======================================================================================
        struct Brush {
            BrushType               type;
            BrushContent            content;
            BrushTextureMode        textureMode;
            math::Quaternion        orientation;
            std::vector<BrushSide>  sides;
        };


        //
        // function declarations
        //

        BrushPosition brush_position_from_vector(math::Vector3 const &in_position);
        math::Vector3 vector_from_brush_position(BrushPosition const &in_position);

        BrushPosition brush_position_from_integer(int64_t const in_x, int64_t const in_y, int64_t const in_z);

        BrushPosition operator +(BrushPosition const &in_a, BrushPosition const &in_b);
        BrushPosition operator -(BrushPosition const &in_a, BrushPosition const &in_b);
        BrushPosition operator >>(BrushPosition const &in_pos, int64_t const in_shift);

        BrushPosition snap_brush_position(BrushPosition const &in_pos, int64_t const in_snap);

        math::Plane plane_from_brush_side(BrushSide const &in_side);

        std::list<Winding> create_brush_windings(Brush const &in_brush, double const in_baseSize);

        Brush create_box_brush(BrushPosition const &in_min, BrushPosition const &in_max);

        bool is_point_inside_brush(Brush const &in_brush, math::Vector3 const &in_point, double const in_epsilon);
        double intersect_ray_with_brush(Brush const &in_brush, math::Ray const &in_ray, double const in_epsilon);
    }
}