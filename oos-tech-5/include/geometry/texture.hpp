/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include <algorithm>

#include "../math/vector.hpp"


namespace oos {
    namespace geometry {
        std::pair<math::Vector3, math::Vector3> texture_axes_for_normal(math::Vector3 const &in_normal);
        std::pair<math::Vector3, math::Vector3> texture_axes_for_normal(math::Vector3 const &in_normal, double const in_angle);

        //math::Vector2 calculate_texture_uv(std::pair<math::Vector3, math::Vector3> const &in_axes, math::Vector3 const &in_point);
        //math::Vector2 calculate_texture_st(math::Vector2 const &in_uv, math::Vector2 const &in_scale, math::Vector2 const &in_translation);
    }
}