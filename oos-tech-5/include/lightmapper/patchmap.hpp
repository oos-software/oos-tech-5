/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include "../renderer/face.hpp"


namespace oos {
    struct Patch;


    // ======================================================================================
    //
    //  PatchVisibility
    //
    // ======================================================================================
    struct PatchVisibility {
        Patch const  *patch;
        double       weight;
    };


    // ======================================================================================
    //
    //  Patch
    //
    // ======================================================================================
    struct Patch {
        math::Vector3               position;            // center of patch in world space
        math::Vector3               corners[4];          // the corners (needed to calculate the form factors)
        
        math::Vector3               directIllumination;  // direct light arriving at patch
        math::Vector3               illumination;        // total (direct + indirect) light arriving at patch
        math::Vector3               radiosity;           // light leaving patch
        math::Vector3               tempRadiosity;       // used during a bounce
        math::Vector3               reflectivity;        // the amount of light which is reflected back into the scene
        
        bool                        isInsideFace;        // true if inside the owner face
        bool                        isInsideBrush;       // true if inside any brush
        bool                        hasSunlight;         // this is set for directional lights to perform shadow filtering later on

        std::list<PatchVisibility>  visibility;          // precalculated visibility for the patch
    };


    // ======================================================================================
    //
    //  PatchMap
    //
    // ======================================================================================
    struct PatchMap {
        uint16_t            width;
        uint16_t            height;
        std::vector<Patch>  patches;
        math::Vector3       normal;
    };


    //
    // function declarations
    //

    bool is_patch_valid(Patch const &in_patch);

    PatchMap patch_map_from_face(Face &in_face, std::vector<geometry::Brush> const &in_brushes, double const in_resolution, int const in_border);

    void perform_dilatation(PatchMap &in_patchMap);

    bool write_patch_map_png(PatchMap const &in_patchMap, std::string const &in_fileName);
}