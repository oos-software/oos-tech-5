/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once





namespace oos {
    class World;


    // ======================================================================================
    //
    //  LightmapperQuality
    //
    // ======================================================================================
    enum class LightmapperQuality {
        Low,
        Medium,
        High
    };


    //
    // function declarations
    //

    void rebuild_world_lighting(World *in_world, LightmapperQuality const in_quality);
}