/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include <string>
#include <vector>
#include <functional>
#include <unordered_map>


namespace oos {
    // ======================================================================================
    //
    //  XmlAttribute
    //
    // ======================================================================================
    struct XmlAttribute {
        virtual std::string value() const = 0;
    };


    // ======================================================================================
    //
    //  XmlNode
    //
    // ======================================================================================
    struct XmlNode {
        virtual std::string name() const = 0;
        virtual std::string value() const = 0;

        virtual std::string attribute(std::string const &in_name) const = 0;
        virtual std::string value(std::string const &in_name) const = 0;

        virtual void for_attributes(std::function<void(XmlAttribute const &)> const &in_callback) const = 0;
        virtual void for_nodes(std::function<void(XmlNode const &)> const &in_callback) const = 0;

        virtual void for_nodes(std::string const &in_name, std::function<void(XmlNode const &)> const &in_callback) const = 0;
    };


    // ======================================================================================
    //
    //  XmlDocument
    //
    // ======================================================================================
    struct XmlDocument {
        virtual ~XmlDocument() = default;

        virtual XmlNode const *root_node() const = 0;


    public:
        static XmlDocument *from_file(std::string const &in_fileName);
        static XmlDocument *from_memory(std::vector<uint8_t> const &in_data);
    };


    // ======================================================================================
    //
    //  XmlMapping
    //
    // ======================================================================================
    template <class T> class XmlMapping {
    public:
        void add_node(std::string const &in_name, XmlMapping<T> const &in_mapping) {
            m_nodes[in_name] = in_mapping;
        }

        void add_attributes(std::string const &in_name, std::function<void(T *, std::string const &)> const &in_handler) {
            m_attributes[in_name] = in_handler;
        }

        void add_property(std::string const &in_name, std::function<void(T *, std::string const &)> const &in_handler) {
            m_properties[in_name] = in_handler;
        }

        void process(T *in_object, XmlNode const &in_node) const {
            in_node.for_attributes([this, in_object](XmlAttribute const &attrib) {
                auto const  value = attrib.value();
                auto        it = m_attributes.find(value);
                if (it != m_attributes.end()) {
                    it->second(in_object, value);
                }
            });

            in_node.for_nodes([this, in_object](XmlNode const &inner) {
                auto const  name = inner.name();

                auto  propIt = m_properties.find(name);
                if (propIt != m_properties.end()) {
                    propIt->second(in_object, inner.value());
                    return;
                }

                auto  nodeIt = m_nodes.find(name);
                if (nodeIt != m_nodes.end()) {
                    nodeIt->second.process(in_object, inner);
                }
            });
        }


    private:
        std::unordered_map<std::string, XmlMapping<T>>                                  m_nodes;
        std::unordered_map<std::string, std::function<void(T *, std::string const &)>>  m_attributes;
        std::unordered_map<std::string, std::function<void(T *, std::string const &)>>  m_properties;
    };
}