/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include <tuple>

#include "math/matrix.hpp"


namespace oos {
    // ======================================================================================
    //
    //  View
    //
    // ======================================================================================
    struct View {
        math::Vector3 const &origin() const {
            return(m_origin);
        }

        void origin(math::Vector3 const &in_origin) {
            m_origin = in_origin;
        }

        math::Vector3 const &angles() const {
            return(m_angles);
        }

        void angles(math::Vector3 const &in_angles) {
            m_angles = in_angles;
        }

        double near_clip() const {
            return(m_nearClip);
        }

        void near_clip(double const in_nearClip) {
            m_nearClip = in_nearClip;
        }

        double far_clip() const {
            return(m_farClip);
        }

        void far_clip(double const in_farClip) {
            m_farClip = in_farClip;
        }

        math::Matrix4 calculate_view_matrix() const;
        math::Matrix4 calculate_view_projection_matrix() const;

        virtual math::Matrix4 calculate_projection_matrix() const = 0;


    private:
        math::Vector3  m_origin;
        math::Vector3  m_angles;
        double         m_nearClip;
        double         m_farClip;
    };


    // ======================================================================================
    //
    //  PerspectiveView
    //
    // ======================================================================================
    struct PerspectiveView : public View {
        PerspectiveView(double const in_fov, double const in_aspect);

        double horizontal_fov() const {
            return(m_fov);
        }

        void horizontal_fov(double const in_fov) {
            m_fov = in_fov;
        }

        double aspect() const {
            return(m_aspect);
        }

        void aspect(double const in_aspect) {
            m_aspect = in_aspect;
        }

        math::Matrix4 calculate_projection_matrix() const;


    private:
        double  m_fov;
        double  m_aspect;
    };


    // ======================================================================================
    //
    //  OrthographicView
    //
    // ======================================================================================
    struct OrthographicView : public View {
        OrthographicView(double const in_left, double const in_bottom, double const in_right, double const in_top);

        math::Matrix4 calculate_projection_matrix() const;


    private:
        double const  m_left;
        double const  m_bottom;
        double const  m_right;
        double const  m_top;
    };


    //
    // function declarations
    //

    std::tuple<math::Vector3, math::Vector3, math::Vector3> view_vectors_from_view(View const &in_view);
    std::tuple<math::Vector3, math::Vector3, math::Vector3> view_vectors_from_view_matrix(math::Matrix4 const &in_matrix);
}