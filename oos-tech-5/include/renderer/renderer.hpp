/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


namespace oos {
    class World;


    // ======================================================================================
    //
    //  RenderSystem
    //
    // ======================================================================================
    struct RenderSystem {
        Texture *create_single_color_texture(RenderDevice *in_renderDevice, uint8_t const in_r, uint8_t const in_g, uint8_t const in_b, uint8_t const in_a = 255);
    };


    // ======================================================================================
    //
    //  RenderWorld
    //
    // ======================================================================================
    class RenderWorld {
    public:
        RenderWorld(RenderDevice *in_renderDevice, World const *in_world);
        ~RenderWorld();

        Texture *get_light_map(uint32_t const in_index) {
            if (in_index >= m_lightMaps.size()) {
                return(m_lightMaps[0]);
            }
            return(m_lightMaps[in_index]);
        }
        

    private:
        void create_light_maps(World const *in_world);


    private:
        RenderDevice            *m_renderDevice;

        // there is always one lightmap at index 0 which has full (1, 1, 1) brightness
        std::vector<Texture *>  m_lightMaps;
    };
}