/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once

/*
everything is drawn with the same vertex
*/

namespace oos {
    // ======================================================================================
    //
    //  DrawVertex
    //
    // ======================================================================================
    struct DrawVertex {
        float  position[4];  // homogenous coordinate
        float  normal[4];    // 1 coordinate unused
        float  st[4];        // texture, light map
    };


    // ======================================================================================
    //
    //  DrawBatch
    //
    // ======================================================================================
    struct DrawBatch {
        uint32_t  firstIndex;
        uint32_t  indexCount;
    };
}