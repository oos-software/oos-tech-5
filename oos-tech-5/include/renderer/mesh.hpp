/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include <list>

#include <renderdevice.hpp>

#include "draw.hpp"
#include "face.hpp"


namespace oos {
    // ======================================================================================
    //
    //  MeshBuilder
    //
    // ======================================================================================
    class MeshBuilder {
    public:
        DrawBatch add_face(Face const &in_face);
        DrawBatch add_faces(std::list<Face> const &in_faces);

        VertexArray *create_vertex_array(RenderDevice *in_renderDevice) const;


    private:
        std::list<DrawVertex>  m_vertexes;
        std::list<uint32_t>    m_indexes;
    };
}