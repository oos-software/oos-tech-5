/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include <geometry/winding.hpp>


namespace oos {
    // ======================================================================================
    //
    //  FaceVertex
    //
    // ======================================================================================
    struct FaceVertex {
        uint32_t       index;
        math::Vector3  position;
        math::Vector3  normal;
        math::Vector2  texture;
        math::Vector2  lightMap;
    };


    // ======================================================================================
    //
    //  Face
    //
    // ======================================================================================
    struct Face {
        math::Plane               plane;
        std::vector<FaceVertex>   vertexes;
        std::vector<math::Plane>  edgePlanes;
        uint32_t                  lightMap;
    };


    //
    // function declarations
    //

    Face face_from_winding(geometry::Winding const &in_winding);
    bool is_point_inside_face(Face const &in_face, math::Vector3 const &in_point, double const in_epsilon);
}