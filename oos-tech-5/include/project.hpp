/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include <string>


namespace oos {
    // ======================================================================================
    //
    //  Project
    //
    // ======================================================================================
    struct Project {
        static std::string get_material_file_name(std::string const &in_name);
        static std::string get_sound_file_name(std::string const &in_name);
        static std::string get_music_file_name(std::string const &in_name);
        static std::string get_level_file_name(std::string const &in_name);
    };
}