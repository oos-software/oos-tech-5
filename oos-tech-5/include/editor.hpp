/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include "world/world.hpp"


namespace oos {
    // ======================================================================================
    //
    //  EditorImport
    //
    // ======================================================================================
    struct EditorImport {
        // world
        virtual World *load_world(std::string const &in_name) = 0;
        virtual bool save_world(World *in_world, std::string const &in_name) = 0;
        virtual void rebuild_world(World *in_world) = 0;
        virtual void draw_world(World *in_world) = 0;
    };


    //
    // API
    //

    __declspec(dllexport) EditorImport *create_editor_import();
    __declspec(dllexport) void destroy_editor_import(EditorImport *in_import);
}