/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#pragma once


#include "../math/vector.hpp"


namespace oos {
    // ======================================================================================
    //
    //  Bounding
    //
    // ======================================================================================
    class Bounding {
    public:
        Bounding();
        Bounding(math::Vector3 const &in_min, math::Vector3 const &in_max);

        math::Vector3 const &min() const {
            return(m_min);
        }

        math::Vector3 const &max() const {
            return(m_max);
        }

        math::Vector3 center() const;
        math::Vector3 size() const;

        Bounding to_global(math::Vector3 const &in_origin) const;
        Bounding to_local(math::Vector3 const &in_origin) const;

        Bounding operator +(Bounding const &in_other) const;


    private:
        math::Vector3  m_min;
        math::Vector3  m_max;
    };
}