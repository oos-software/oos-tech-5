/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <math/math.hpp>
#include <renderer/view.hpp>


namespace oos {
    // ======================================================================================
    //
    //  MaterialFlag
    //
    // ======================================================================================
    enum class MaterialFlag {
        NoDraw = 0x01,    // does not draw inside the game (only inside the editor)
        NoDecals = 0x02,  // no decals (bullet holes, blood etc.)
        TwoSided = 0x04,  // no back-face culling
    };


    // ======================================================================================
    //
    //  Material
    //
    // ======================================================================================
    struct Material {


        //Texture  *albedo;
        //Texture  *normal;
    };

}