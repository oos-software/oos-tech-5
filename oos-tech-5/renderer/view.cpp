/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <math/math.hpp>
#include <renderer/view.hpp>


namespace oos {
    // ======================================================================================
    //
    //  View
    //
    // ======================================================================================

    //
    // calculate_view_matrix
    //

    math::Matrix4 View::calculate_view_matrix() const {
        auto const  translation = math::translation_matrix(m_origin.negated());

        auto const  rotateX = math::rotation_matrix(0, -m_angles.x());
        auto const  rotateY = math::rotation_matrix(1, -m_angles.y());
        auto const  rotateZ = math::rotation_matrix(2, -m_angles.z());
        auto const  rotation = math::to_matrix4(rotateY * rotateZ * rotateX);

        return(translation * rotation);
    }


    //
    // calculate_view_projection_matrix
    //

    math::Matrix4 View::calculate_view_projection_matrix() const {
        auto const  view = calculate_view_matrix();
        auto const  projection = calculate_projection_matrix();
        return(view * projection);
    }


    // ======================================================================================
    //
    //  PerspectiveView
    //
    // ======================================================================================

    //
    // constructor
    //

    PerspectiveView::PerspectiveView(double const in_fov, double const in_aspect) :
    m_fov(in_fov),
    m_aspect(in_aspect) {
        near_clip(1.0);
        far_clip(10000.0);
    }


    //
    // calculate_projection_matrix
    //

    math::Matrix4 PerspectiveView::calculate_projection_matrix() const {
        auto const  nc = near_clip();
        auto const  fc = far_clip();
        auto const  right = nc * tan(m_fov / 360.0 * math::PI);
        auto const  top = right / m_aspect;

        return(math::Matrix4(
            nc / right, 0, 0, 0,
            0, 0, nc / top, 0,
            0, (fc + nc) / (fc - nc), 0, (-2.0 * fc * nc) / (fc - nc),
            0, 1, 0, 0));
    }


    // ======================================================================================
    //
    //  OrthographicView
    //
    // ======================================================================================

    //
    // constructor
    //

    OrthographicView::OrthographicView(double const in_left, double const in_bottom, double const in_right, double const in_top) :
    m_left(in_left),
    m_bottom(in_bottom),
    m_right(in_right),
    m_top(in_top) {
        near_clip(-1.0);
        far_clip(1.0);
    }


    //
    // calculate_projection_matrix
    //

    math::Matrix4 OrthographicView::calculate_projection_matrix() const {
        auto const  nc = near_clip();
        auto const  fc = far_clip();
        
        return(math::Matrix4(
            2 / (m_right - m_left), 0, 0, -((m_right + m_left) / (m_right - m_left)),
            0, 0, 2 / (m_top - m_bottom), -((m_top + m_bottom) / (m_top - m_bottom)),
            0, -2 / (fc - nc), 0, -((fc + nc) / (fc - nc)),
            0, 0, 0, 1));
    }


    //
    // view_vectors_from_view
    //

    std::tuple<math::Vector3, math::Vector3, math::Vector3> view_vectors_from_view(View const &in_view) {
        return(view_vectors_from_view_matrix(in_view.calculate_view_matrix()));
    }


    //
    // view_vectors_from_view_matrix
    //

    std::tuple<math::Vector3, math::Vector3, math::Vector3> view_vectors_from_view_matrix(math::Matrix4 const &in_matrix) {
        auto const  right = math::Vector3(in_matrix[0], in_matrix[1], in_matrix[2]);
        auto const  normal = math::Vector3(in_matrix[4], in_matrix[5], in_matrix[6]);
        auto const  up = math::Vector3(in_matrix[8], in_matrix[9], in_matrix[10]);
        return(std::make_tuple(right, normal, up));
    }
}