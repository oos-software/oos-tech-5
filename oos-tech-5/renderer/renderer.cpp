/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <renderdevice.hpp>
#include <renderer/renderer.hpp>
#include <world/world.hpp>


namespace oos {
    // ======================================================================================
    //
    //  RenderSystem
    //
    // ======================================================================================

    //
    // create_single_color_texture
    //

    Texture *RenderSystem::create_single_color_texture(RenderDevice *in_renderDevice, uint8_t const in_r, uint8_t const in_g, uint8_t const in_b, uint8_t const in_a) {
        TextureSettings  settings;
        uint8_t const    pixel[4] = { in_r, in_g, in_b, in_a };
        settings.data = pixel;
        return(in_renderDevice->create_texture(settings));
    }


    // ======================================================================================
    //
    // RenderWorld
    //
    // ======================================================================================

    //
    // constructor
    //

    RenderWorld::RenderWorld(RenderDevice *in_renderDevice, World const *in_world) :
    m_renderDevice(in_renderDevice) {
        create_light_maps(in_world);
    }


    //
    // destructor
    //

    RenderWorld::~RenderWorld() {
        for (auto lm : m_lightMaps) {
            m_renderDevice->destroy_texture(lm);
        }
    }


    //
    // create_light_maps
    //

    void RenderWorld::create_light_maps(World const *in_world) {/*
        TextureSettings  settings;
        settings.numComponents = 3;
        settings.format = TextureFormat::Float32;
        settings.filter = TextureFilter::Anisotropic;  // TODO: variable
        settings.wrap = TextureWrap::Clamp;
        
        m_lightMaps.resize(1 + in_world->get_light_maps().size());

        float const  bright[3] = { 1.0f, 1.0f, 1.0f };
        settings.data = bright;
        m_lightMaps[0] = m_renderDevice->create_texture(settings);

        for (size_t i = 0; i < in_world->get_light_maps().size(); ++i) {
            auto const  &lm = in_world->get_light_maps()[i];
            settings.width = lm.width;
            settings.height = lm.height;
            settings.data = lm.luxels.data();
            m_lightMaps[i + 1] = m_renderDevice->create_texture(settings);
        }*/
    }
}