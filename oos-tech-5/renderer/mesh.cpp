/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <renderer/mesh.hpp>


namespace oos {
    // ======================================================================================
    //
    //  MeshBuilder
    //
    // ======================================================================================

    //
    // add_face
    //

    DrawBatch MeshBuilder::add_face(Face const &in_face) {
        auto const  firstIndex = uint32_t(m_indexes.size());
        auto const  startVertex = uint32_t(m_vertexes.size());

        // vertexes
        for (auto const &v : in_face.vertexes) {
            DrawVertex  vertex;

            vertex.position[0] = float(v.position.x());
            vertex.position[1] = float(v.position.y());
            vertex.position[2] = float(v.position.z());
            vertex.position[3] = 1.0f;

            vertex.normal[0] = float(v.normal.x());
            vertex.normal[1] = float(v.normal.y());
            vertex.normal[2] = float(v.normal.z());
            vertex.normal[3] = 0.0f;

            vertex.st[0] = float(v.texture.x());
            vertex.st[1] = float(v.texture.y());
            vertex.st[2] = float(v.lightMap.x());
            vertex.st[3] = float(v.lightMap.y());

            m_vertexes.push_back(vertex);
        }

        // indexes
        for (uint32_t i = 1; i < in_face.vertexes.size() - 1; ++i) {
            m_indexes.push_back(startVertex);
            m_indexes.push_back(startVertex + i);
            m_indexes.push_back(startVertex + i + 1);
        }

        return { firstIndex, uint32_t(m_indexes.size()) - firstIndex };
    }


    //
    // add_faces
    //

    DrawBatch MeshBuilder::add_faces(std::list<Face> const &in_faces) {
        DrawBatch  result;
        result.firstIndex = uint32_t(m_indexes.size());
        result.indexCount = 0;

        for (auto const &face : in_faces) {
            auto const  temp = add_face(face);
            result.indexCount += temp.indexCount;
        }

        return(result);
    }


    //
    // create_vertex_array
    //

    VertexArray *MeshBuilder::create_vertex_array(RenderDevice *in_renderDevice) const {
        VertexArraySettings  settings;
        
        settings.attributes = { { 4, VertexFormat::Float32 }, { 4, VertexFormat::Float32 }, { 4, VertexFormat::Float32 } };  // position, normal, st

        auto const  vertexes = std::vector<DrawVertex>(m_vertexes.begin(), m_vertexes.end());
        settings.numVertexes = uint32_t(m_vertexes.size());
        settings.vertexes = vertexes.data();

        auto const  indexes = std::vector<uint32_t>(m_indexes.begin(), m_indexes.end());
        settings.numIndexes = uint32_t(m_indexes.size());
        settings.indexes = indexes.data();

        return(in_renderDevice->create_vertex_array(settings));
    }
}