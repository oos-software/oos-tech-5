/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <algorithm>

#include <renderer/face.hpp>


namespace oos {
    //
    // face_from_winding
    //

    Face face_from_winding(geometry::Winding const &in_winding) {
        Face  result;
        result.plane = geometry::plane_from_winding(in_winding);

        result.vertexes.reserve(in_winding.size());
        std::transform(in_winding.begin(), in_winding.end(), std::back_inserter(result.vertexes), [&result](math::Vector3 const &pos) {
            FaceVertex  vertex;
            vertex.position = pos;
            vertex.normal = result.plane.normal();
            return(vertex);
        });

        result.edgePlanes.reserve(in_winding.size());
        for (size_t i = in_winding.size() - 1, j = 0; j < in_winding.size(); i = j++) {
            auto const  normal = math::cross(in_winding[j] - in_winding[i], result.plane.normal()).normalized();
            result.edgePlanes.push_back(math::Plane(normal, in_winding[i]));
        }

        result.lightMap = 0;
        return(result);
    }


    //
    // is_point_inside_face
    //
    
    bool is_point_inside_face(Face const &in_face, math::Vector3 const &in_point, double const in_epsilon) {
        for (auto const &plane : in_face.edgePlanes) {
            if (plane.distance_from(in_point) > in_epsilon) {
                return(false);
            }
        }
        return(true);
    }
}