/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <project.hpp>

#include "../third-party/s7/s7.h"


namespace oos {
    s7_scheme *get_scheme();


    //
    // get_asset_file_name
    //

    static std::string get_asset_file_name(std::string const &in_method, std::string const &in_name) {
        auto  sc = get_scheme();
        auto  result = s7_string(s7_call(sc, s7_name_to_value(sc, in_method.c_str()), s7_cons(sc, s7_make_string(sc, in_name.c_str()), s7_nil(sc))));
        if (nullptr == result) {
            return("");
        }
        return(result);
    }


    // ======================================================================================
    //
    //  Project
    //
    // ======================================================================================
    
    //
    // get_material_file_name
    //

    std::string Project::get_material_file_name(std::string const &in_name) {
        return(get_asset_file_name("get-material-file-name", in_name));
    }


    //
    // get_sound_file_name
    //

    std::string Project::get_sound_file_name(std::string const &in_name) {
        return(get_asset_file_name("get-sound-file-name", in_name));
    }


    //
    // get_music_file_name
    //

    std::string Project::get_music_file_name(std::string const &in_name) {
        return(get_asset_file_name("get-music-file-name", in_name));
    }


    //
    // get_level_file_name
    //

    std::string Project::get_level_file_name(std::string const &in_name) {
        return(get_asset_file_name("get-level-file-name", in_name));
    }
}