/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <framework.hpp>
#include <input.hpp>


namespace oos {
    // ======================================================================================
    //
    //  InputFlag
    //
    // ======================================================================================
    enum class InputFlag {
        Press = 0x01,
        Release = 0x02,
        Down = 0x04
    };


    // ======================================================================================
    //
    //  InputSystem
    //
    // ======================================================================================
    /*
    //
    // register_binding
    //

    void InputSystem::register_binding(std::string const &in_name, InputKey const in_firstKey, InputKey const in_secondKey) {

    }


    //
    // write_bindings
    //

    void InputSystem::write_bindings() {

    }
    */

    //
    // is_key_press
    //

    bool InputSystem::is_key_press(InputKey const in_key) const {
        return(0 != (m_keyFlags[int(in_key)] & int(InputFlag::Press)));
    }


    //
    // is_key_release
    //

    bool InputSystem::is_key_release(InputKey const in_key) const {
        return(0 != (m_keyFlags[int(in_key)] & int(InputFlag::Release)));
    }

    
    //
    // is_key_down
    //

    bool InputSystem::is_key_down(InputKey const in_key) const {
        return(0 != (m_keyFlags[int(in_key)] & int(InputFlag::Down)));
    }


    //
    // update
    //

    void InputSystem::update(InputDevice *in_inputDevice) {
        // reset one-time flags
        for (int i = 0; i < int(InputKey::NumInputKeys); ++i) {
            m_keyFlags[i] &= ~(int(InputFlag::Press) | int(InputFlag::Release));
        }

        // process all events from the device
        in_inputDevice->process_events([this](InputEvent const &ev) {
            if (InputAction::Press == ev.action) {
                m_keyFlags[int(ev.key)] |= int(InputFlag::Press) | int(InputFlag::Down);
            }
            else {
                m_keyFlags[int(ev.key)] |= int(InputFlag::Release);
                m_keyFlags[int(ev.key)] &= ~(int(InputFlag::Down));
            }
        });
    }
}