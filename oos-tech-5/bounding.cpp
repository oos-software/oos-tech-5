/*----------------------------------------------------------------------------------------------
 * Copyright (c) Holger Meisel. All rights reserved.
 * Licensed under the MIT License. See LICENSE.txt in the project root for license information.
 *----------------------------------------------------------------------------------------------*/

#include <bounding.hpp>


namespace oos {
    static double constexpr  Infinity = std::numeric_limits<double>::infinity();


    // ======================================================================================
    //
    //  Bounding
    //
    // ======================================================================================

    //
    // constructor
    //

    Bounding::Bounding() :
    Bounding(math::Vector3(Infinity, Infinity, Infinity), math::Vector3(-Infinity, -Infinity, -Infinity)) {

    }

    Bounding::Bounding(math::Vector3 const &in_min, math::Vector3 const &in_max) :
    m_min(in_min),
    m_max(in_max) {

    }


    //
    // center
    //

    math::Vector3 Bounding::center() const {
        return((m_min + m_max) * 0.5);
    }


    //
    // size
    //

    math::Vector3 Bounding::size() const {
        return((m_max - m_min) * 0.5);
    }


    //
    // to_global
    //

    Bounding Bounding::to_global(math::Vector3 const &in_origin) const {
        return(Bounding(m_min + in_origin, m_max + in_origin));
    }


    //
    // to_local
    //

    Bounding Bounding::to_local(math::Vector3 const &in_origin) const {
        return(Bounding(m_min - in_origin, m_max - in_origin));
    }


    //
    // + (union)
    //

    Bounding Bounding::operator +(Bounding const &in_other) const {
        double  min[3];
        double  max[3];
        for (int i = 0; i < 3; ++i) {
            min[i] = std::min(m_min[i], in_other.m_min[i]);
            max[i] = std::max(m_max[i], in_other.m_max[i]);
        }
        return(Bounding(math::Vector3(min), math::Vector3(max)));
    }
}